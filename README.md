# Semestrální práce MI-APH

## Spuštění hry

```
npm install
npm start
firefox localhost:1234
```

## Manuál
[pdf](manual.pdf)

## Implementace
Pro implementaci byla použita poskytnutá ECSA knihovna (pixi-component) a knihovna pixi-math (mimo jiné pro steering behaviour). 
Dále byly použity poskytnuté soubory dynamics.ts a functions.ts, které byly modifikovány podle potřeby. 

## Použité assety

Pro tvorbu hry byly použity následující assety. 

------------------------------------------------------------------------------------
audio/effects/laser4.mp3, audio/effects/laser5.mp3, 
audio/effects/powerUp2.mp3, audio/effects/zapThreeToneDown.mp3
(all converted from .ogg)

gfx/spritesheets/* (sheet.json converted from sheet.xml), 
gfx/images/*

by Kenney Vleugels ([Kenney.nl](https://www.kenney.nl/))

License (Creative Commons Zero, CC0)
http://creativecommons.org/publicdomain/zero/1.0/

https://www.kenney.nl/assets/digital-audio
https://www.kenney.nl/assets/space-shooter-redux
https://www.kenney.nl/assets/space-shooter-extension

------------------------------------------------------------------------------------
audio/effects/explode.mp3, audio/effects/explodemini.mp3, audio/effects/launch.mp3
(converted from .wav)

Sounds (c) by Michel Baradari apollo-music.de

Licensed under CC BY 3.0 http://creativecommons.org/licenses/by/3.0/

Hosted on opengameart.org
https://opengameart.org/content/2-high-quality-explosions
https://opengameart.org/content/4-projectile-launches

------------------------------------------------------------------------------------
audio/effects/hit.mp3, audio/effects/hit_asteroid.mp3
(converted from .ogg)

by rubberduck (https://opengameart.org/users/rubberduck)

License (Creative Commons Zero, CC0)
http://creativecommons.org/publicdomain/zero/1.0/

Hosted on opengameart.org (https://opengameart.org/content/75-cc0-breaking-falling-hit-sfx)

------------------------------------------------------------------------------------
audio/music/through space.mp3
(converted from .ogg)

by maxstack (https://opengameart.org/users/maxstack)

Licensed under CC BY-SA 3.0 https://creativecommons.org/licenses/by-sa/3.0/

Hosted on opengameart.org (https://opengameart.org/content/through-space)

------------------------------------------------------------------------------------
gfx/background/Stars.png, 
gfx/background/Nebula3.png

by Skorpio (https://opengameart.org/users/skorpio)

Licensed under CC BY-SA 3.0 https://creativecommons.org/licenses/by-sa/3.0/

Hosted on opengameart.org (https://opengameart.org/content/space-ship-construction-kit)
