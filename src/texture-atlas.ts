/**
 * Texture atlas class for accessing textures from sheet
 */
export class TextureAtlas {
  private textures: PIXI.Texture;
  private textureAtlas: Map<string, PIXI.Rectangle>;

  constructor(textures: PIXI.Texture, textureAtlasData: any) {
    this.textures = textures;
    this.initTextureAtlas(textureAtlasData);
  }

  /**
   * Returns texture from texture atlas
   * @param name Texture name, names are defined in sheet.json
   */
  getTexture(name: string): PIXI.Texture {
    if (!this.textureAtlas.has(name)) {
      throw new Error('Texture not found: "' + name + '"');
    }
    let texture = this.textures.clone();
    texture.frame = this.textureAtlas.get(name);
    return texture;
  }

  private initTextureAtlas(textureAtlasData: any) {
    this.textureAtlas = new Map();
    for (let entry of textureAtlasData['SubTexture']) {
      // get texture rectangle from texture atlas
      /* note: unfortunately, textures' rectangles defined in provided sheet
         seem to be slightly off, so we cut 1 pixel wide strip from each side
         of the texture to prevent visual artefacts */
      this.textureAtlas.set(entry['_name'],
        new PIXI.Rectangle(+entry['_x'] + 1, +entry['_y'] + 1,
          +entry['_width'] - 2, +entry['_height'] - 2));
    }
  }
}
