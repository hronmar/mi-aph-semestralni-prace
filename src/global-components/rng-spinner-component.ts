import { BaseComponent } from "../components/base-component";

/**
 * Component that performs RNG spinning, it calls random generator each frame
 */
export class RNGSpinnerComponent extends BaseComponent {
  onUpdate(delta: number, absolute: number) {
    this.random.float();
  }
}
