import * as ECSA from '../../libs/pixi-component';
import { BaseComponent } from '../components/base-component';
import { Messages, Attributes, Flags } from '../constants';
import { ObjectDestroyedMessage } from '../components/destructible';
import { DestructionTriggerBase } from '../components/destruction-trigger-base';

/**
 * Component that reacts to OBJECT_DESTROYED messages and performs objects destruction
 */
export class ObjectDestructionManager extends BaseComponent {
  onInit() {
    super.onInit();
    this.subscribe(Messages.OBJECT_DESTROYED);
  }

  onMessage(msg: ECSA.Message) {
    if (msg.action === Messages.OBJECT_DESTROYED) {
      let target = (msg.data as ObjectDestroyedMessage).target;

      if (target.hasFlag(Flags.DESTRUCTION_TRIGGER)) {
        this.executeTriggers(target);
      }

      this.removeObject(target);
    }
  }

  private executeTriggers(obj: ECSA.Container) {
    let triggers = obj.getAttribute<Set<DestructionTriggerBase>>(Attributes.DESTRUCTION_TRIGGERS);
    for (let trigger of triggers) {
      trigger.execute();
    }
  }
}
