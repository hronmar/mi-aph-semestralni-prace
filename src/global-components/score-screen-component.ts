import * as ECSA from '../../libs/pixi-component';
import { Attributes, AppState, Names } from '../constants'
import { AppStateManager } from '../app-state-manager';
import { BaseComponent } from '../components/base-component';

/**
 * Component to display and control score screen
 */
export class ScoreScreenComponent extends BaseComponent {
  private score: number;

  constructor(score: number) {
    super();
    this.score = score;
  }

  onInit() {
    super.onInit();
    let highscore = +window.localStorage.getItem(Names.HIGHSCORE);
    let record = false;
    if (this.score > highscore) {
      record = true;
      highscore = this.score;
    }
    window.localStorage.setItem(Names.HIGHSCORE, highscore.toString());
    this.factory.createScoreScreenDisplay(this.scene, this.score, highscore, record);
  }

  onUpdate(delta: number, absolute: number) {
    if (this.input.isKeyPressed(ECSA.Keys.KEY_SPACE)) {
      // go to menu
      this.scene.invokeWithDelay(5,
        () => this.scene.getGlobalAttribute<AppStateManager>(Attributes.APP_STATE_MANAGER)
          .switchToState(AppState.MENU));
    }
  }
}
