import * as ECSA from '../../libs/pixi-component';
import { BaseComponent } from '../components/base-component';
import { Messages, Flags, Names, Attributes, SCENE_WIDTH, SCENE_HEIGHT } from '../constants';
import { CollisionMessage } from './collision-manager';
import { Destructible } from '../components/destructible';
import { CollectibleItem } from '../components/collectible';
import { rectanglesIntersect } from '../utils/functions';

/**
 * Component that handles collisions between collidable objects
 */
export class CollisionResolver extends BaseComponent {
  private sceneBoundingBox: PIXI.Rectangle;

  onInit() {
    this.sceneBoundingBox = new PIXI.Rectangle(0, 0, SCENE_WIDTH, SCENE_HEIGHT);
    super.onInit();
    this.subscribe(Messages.OBJECTS_COLLISION);
  }

  onMessage(msg: ECSA.Message) {
    if (msg.action === Messages.OBJECTS_COLLISION) {
      let data = msg.data as CollisionMessage;
      let obj1 = data.object1;
      let obj2 = data.object2;

      if (!rectanglesIntersect(obj1.getBounds(), this.sceneBoundingBox)
        || !rectanglesIntersect(obj2.getBounds(), this.sceneBoundingBox)) {
        return; // ignore collisions outside scene
      }

      if (obj1.hasFlag(Flags.COLLECTIBLE) || obj2.hasFlag(Flags.COLLECTIBLE)) {
        this.handleCollectible(obj1, obj2);
      } else {
        this.collideObjects(obj1, obj2);
      }
    }
  }

  private collideObjects(obj1: ECSA.Container, obj2: ECSA.Container) {
    // if objects are destructible, inflict impact damage
    if (obj1.hasFlag(Flags.DESTRUCTIBLE)) {
      (obj1.findComponentByName<Destructible>(Destructible.name))
        .impactBy(obj2);
    }
    if (obj2.hasFlag(Flags.DESTRUCTIBLE)) {
      (obj2.findComponentByName<Destructible>(Destructible.name))
        .impactBy(obj1);
    }
  }

  private handleCollectible(obj1: ECSA.Container, obj2: ECSA.Container) {
    if (obj1.name !== Names.PLAYER_SHIP) {
      // ensure player's ship is obj1 and collectible is obj2
      if (obj2.name === Names.PLAYER_SHIP) {
        let tmp = obj1;
        obj1 = obj2;
        obj2 = tmp;
      } else {
        // not collected by player's ship
        return;
      }
    }

    // collect item obj2 by player
    let item = obj2.getAttribute<CollectibleItem>(Attributes.COLLECTIBLE_ITEM);
    this.sendMessage(Messages.ITEM_COLLECTED, item);
    obj2.remove();
  }
}
