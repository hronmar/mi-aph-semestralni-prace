import * as ECSA from '../../libs/pixi-component';
import { BaseComponent } from '../components/base-component';
import { checkTime } from '../utils/functions';
import { SCENE_WIDTH, ASTEROID_SPEED_MIN, ASTEROID_SPEED_MAX, AsteroidSize, AsteroidType, SCENE_HEIGHT, Names, INTERCEPTOR_SPEED, MissileType } from '../constants';
import Dynamics from '../utils/dynamics';
import { CollectibleItem } from '../components/collectible';

const FREQUENCY_DEFAULT = 0.4;
const FREQUENCY_HIGH = 0.75;
const FREQUENCY_LOW = 0.15;
const SPAWN_BOUNDS_PADDING = new ECSA.Vector(40, 60);

/**
 * Represents set of objects from which spawned objects are picked
 */
class SpawnSet {
  private spawns: string[];
  private probabilities: number[];

  /**
   * Constructs a spawn set with given items
   * @param spawns Array of object names that will be in this set
   * @param weights Weights determining probabilities of corresponding items
   */
  constructor(spawns: string[], weights: number[]) {
    this.spawns = spawns;
    this.probabilities = weights;

    // normalize probabilities into [0,1]
    let sum = 0;
    for (let p of this.probabilities) {
      sum += p;
    }
    for (let i = 0; i < this.probabilities.length; i++) {
      this.probabilities[i] /= sum;
    }
  }

  /**
   * Returns random object to spawn from this set (or null if set is empty)
   * @param rn Random number from [0,1] interval
   */
  getRandomSpawnObject(rn: number): string {
    if (this.spawns.length === 0) {
      return null;
    }

    if (rn < 0 || rn > 1) {
      throw new Error('Invalid argument');
    }

    for (let i = 0; i < this.spawns.length; i++) {
      if (rn < this.probabilities[i]) {
        return this.spawns[i];
      }
      rn -= this.probabilities[i];
    }
  }
}

// definitions of spawn sets
const SPAWN_SETS = [new SpawnSet([Names.ASTEROID, Names.ENEMY_UFO], [0.6, 0.4]),
                    new SpawnSet([Names.ASTEROID, Names.ENEMY_INTERCEPTOR], [0.5, 0.5]),
                    new SpawnSet([Names.ASTEROID, Names.ENEMY_INTERCEPTOR, Names.ENEMY_BOMBER], [0.3, 0.3, 0.4]),
                    new SpawnSet([Names.ASTEROID, Names.ENEMY_BOMBER], [0.7, 0.3])];
const SPAWN_SET_DURATION_MS = 30000;

/**
 * Component responsible for spawning game objects into level (enemies, asteroids, bonuses etc.)
 */
export class GameObjectSpawner extends BaseComponent {
  private lastSpawnTime = 0;
  private spawnFrequency = FREQUENCY_DEFAULT;
  private currentSpawnSet: SpawnSet;
  private currentAsteroidFieldType: AsteroidType;
  private lastSpawnSetChange = 0;

  onInit() {
    super.onInit();
    this.nextSpawnSet();
  }

  onUpdate(delta: number, absolute: number) {
    if (absolute - this.lastSpawnSetChange >= SPAWN_SET_DURATION_MS) {
      // change spawn set
      this.nextSpawnSet();
      this.lastSpawnSetChange = absolute;
    }

    if (checkTime(this.lastSpawnTime, absolute, this.spawnFrequency)) {
      // spawn object
      this.lastSpawnTime = absolute;
      let currentSpawn = this.currentSpawnSet.getRandomSpawnObject(this.random.uniform(0, 1));
      switch (currentSpawn) {
        case Names.ASTEROID:
          this.spawnAsteroid();
          break;
        case Names.ENEMY_UFO:
          this.spawnUFO();
          break;
        case Names.ENEMY_INTERCEPTOR:
          this.spawnInterceptors();
          break;
        case Names.ENEMY_BOMBER:
          this.spawnBomber();
          break;
      }
    }
  }

  private nextSpawnSet() {
    let i = this.random.uniformInt(0, SPAWN_SETS.length - 1);
    this.currentSpawnSet = SPAWN_SETS[i];
    this.currentAsteroidFieldType = (this.random.uniform(0, 1) < 0.5 ? AsteroidType.BROWN : AsteroidType.GREY);
  }

  private spawnAsteroid() {
    let position = new ECSA.Vector(this.random.uniformInt(SPAWN_BOUNDS_PADDING.x,
      SCENE_WIDTH - SPAWN_BOUNDS_PADDING.x), -SPAWN_BOUNDS_PADDING.y);
    let speed = this.random.uniform(ASTEROID_SPEED_MIN, ASTEROID_SPEED_MAX);
    let rotation = this.random.uniform(0, 2 * Math.PI);
    let dynamics = new Dynamics(new ECSA.Vector(0, speed));
    this.factory.createAsteroid(this.scene, position, dynamics, rotation,
      AsteroidSize.LARGE, this.currentAsteroidFieldType);
    this.frequency = FREQUENCY_HIGH;
  }

  private getDrop(probability: number): CollectibleItem {
    if (this.random.uniform(0, 1) < (1 - probability)) {
      return null; // nothing
    }
    let drop: CollectibleItem;
    let rn = this.random.uniform(0, 1);
    if (rn <= 0.5) { // 50%
      drop = CollectibleItem.BONUS_SCORE;
    } else if (rn <= 0.8) { // 30%
      drop = CollectibleItem.BONUS_LIVE;
    } else { // 20%
      drop = CollectibleItem.SHIELD_POWER_UP;
    }
    return drop;
  }

  private spawnUFO() {
    let position = new ECSA.Vector(SCENE_WIDTH / 2 * this.random.normal(0.75, 1.25), -SPAWN_BOUNDS_PADDING.y);
    let drop = this.getDrop(0.25);
    this.factory.createEnemyUFO(this.scene, position, new ECSA.Vector(this.random.uniformInt(-SCENE_WIDTH, 2 * SCENE_WIDTH), 2 * SCENE_HEIGHT), drop);
    this.frequency = FREQUENCY_LOW;
  }

  private spawnInterceptors() {
    let variant = this.random.uniformInt(1, 3);
    if (this.random.uniform(0, 1) < 0.3) {
      // spawn interceptor group
      let numberInterceptors = 3;
      let mid = Math.floor(numberInterceptors / 2);
      let spacing = 200;
      for (let i = 0; i < numberInterceptors; i++) {
        let drop = this.getDrop(0.15);
        let position = new ECSA.Vector(SCENE_WIDTH / 2 + (i - mid) * spacing, -SPAWN_BOUNDS_PADDING.y - Math.abs(i - mid) * 50);
        let dynamics = new Dynamics(new ECSA.Vector(0, INTERCEPTOR_SPEED));
        this.factory.createEnemyInterceptor(this.scene, position, dynamics, 0,
          variant, drop);
      }
      this.frequency = FREQUENCY_DEFAULT;
    } else {
      // spawn just one interceptor
      let drop = this.getDrop(0.2);
      let position = new ECSA.Vector(this.random.uniformInt(SPAWN_BOUNDS_PADDING.x,
        SCENE_WIDTH - SPAWN_BOUNDS_PADDING.x), -SPAWN_BOUNDS_PADDING.y);
      let dynamics = new Dynamics(new ECSA.Vector(0, INTERCEPTOR_SPEED));
      this.factory.createEnemyInterceptor(this.scene, position, dynamics, 0,
        variant, drop);
      this.frequency = FREQUENCY_HIGH;
    }
  }

  private spawnBomber() {
    let drop = this.getDrop(0.4);
    let variant = this.random.uniformInt(1, 2);
    let position = new ECSA.Vector(this.random.uniformInt(SPAWN_BOUNDS_PADDING.x,
      SCENE_WIDTH - SPAWN_BOUNDS_PADDING.x), -SPAWN_BOUNDS_PADDING.y);
    let dynamics = new Dynamics(new ECSA.Vector(0, INTERCEPTOR_SPEED));
    let rng = this.random.uniform(0,1);
    if (rng < 0.3) {
      // just laser
      this.factory.createEnemyBomber(this.scene, position, dynamics, 0,
        variant, drop);
    } else if (rng < 0.6) { //30%
      // guided missiles
      this.factory.createEnemyBomber(this.scene, position, dynamics, 0,
        variant, drop, false, MissileType.GUIDED);
    } else { //40%
      // non-guided missiles and (possibly) laser
      this.factory.createEnemyBomber(this.scene, position, dynamics, 0,
        variant, drop, (this.random.uniform(0,1) < 0.5), MissileType.NORMAL);
    }
    this.frequency = FREQUENCY_DEFAULT;
  }
}
