import * as ECSA from '../../libs/pixi-component';
import { Attributes, Messages, Names } from '../constants'
import { BaseComponent } from '../components/base-component';

/**
 * Component that manages display of player's score
 */
export class ScoreDisplayManager extends BaseComponent {
  onInit() {
    super.onInit();
    this.factory.createScoreDisplay(this.scene, new ECSA.Vector(50, 25));
    this.subscribe(Messages.SCORE_CHANGED);
    this.updateScoreDisplay();
  }

  onMessage(msg: ECSA.Message) {
    if (msg.action === Messages.SCORE_CHANGED) {
      this.updateScoreDisplay();
    }
  }

  private updateScoreDisplay() {
    let score = this.scene.getGlobalAttribute<number>(Attributes.PLAYER_SCORE);
    this.scene.findObjectByName(Names.SCORE_DISPLAY).asText().text = '' + score;
  }
}
