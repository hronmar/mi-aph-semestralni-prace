import { BaseComponent } from "../components/base-component";
import { Flags, SCENE_WIDTH, SCENE_HEIGHT } from '../constants';
import { rectanglesIntersect, checkTime } from '../utils/functions';

const DISPOSER_RUN_FREQUENCY = 0.25;
const OUTSIDE_DISTANCE = 100;

/**
 * Component that periodically removes objects (with REMOVE_OUTSIDE_SCENE flag) when they move outside scene
 */
export class OutsideSceneObjectsDisposer extends BaseComponent {
  private boundingBox: PIXI.Rectangle;
  private lastRun: number;

  onInit() {
    super.onInit();
    this.boundingBox = new PIXI.Rectangle(-OUTSIDE_DISTANCE, -OUTSIDE_DISTANCE, 
      SCENE_WIDTH + 2 * OUTSIDE_DISTANCE, SCENE_HEIGHT + 2 * OUTSIDE_DISTANCE);
    this.lastRun = 0;
  }

  onUpdate(delta: number, absolute: number) {
    if (checkTime(this.lastRun, absolute, DISPOSER_RUN_FREQUENCY)) {
      this.lastRun = absolute;
      this.removeObjectsOutsideScene();
    }
  }

  private removeObjectsOutsideScene() {
    let objects = this.scene.findObjectsByFlag(Flags.REMOVE_OUTSIDE_SCENE);

    for (let obj of objects) {
      if (!rectanglesIntersect(obj.getBounds(), this.boundingBox))
        this.removeObject(obj);
    }
  }
}
