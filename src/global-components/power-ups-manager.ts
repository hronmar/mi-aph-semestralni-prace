import * as ECSA from '../../libs/pixi-component';
import { BaseComponent } from '../components/base-component';
import { Messages, Attributes, MAX_PLAYER_LIVES, Names, SHIELD_DURATION } from '../constants';
import { CollectibleItem } from '../components/collectible';
import { ShieldComponent } from '../components/shield-component';

/**
 * Component that handles power-ups given to player
 */
export class PowerUpsManager extends BaseComponent {
  onInit() {
    super.onInit();
    this.subscribe(Messages.ITEM_COLLECTED);
  }

  onMessage(msg: ECSA.Message) {
    if (msg.action === Messages.ITEM_COLLECTED) {
      let item = msg.data as CollectibleItem;
      switch (item) {
        case CollectibleItem.SHIELD_POWER_UP:
          let player = this.scene.findObjectByName(Names.PLAYER_SHIP);
          player.addComponent(new ShieldComponent(SHIELD_DURATION));
          break;
        case CollectibleItem.BONUS_LIVE:
          {
            // grant additional live to player
            let lives = this.scene.getGlobalAttribute<number>(Attributes.PLAYER_LIVES);
            if (++lives > MAX_PLAYER_LIVES) {
              // already have maximum number of lives
              return;
            }
            this.scene.assignGlobalAttribute(Attributes.PLAYER_LIVES, lives);
            this.sendMessage(Messages.PLAYER_LIVES_CHANGED);
            break;
          }
        case CollectibleItem.BONUS_SCORE:
          {
            let score = this.scene.getGlobalAttribute<number>(Attributes.PLAYER_SCORE);
            score += 5;
            this.scene.assignGlobalAttribute(Attributes.PLAYER_SCORE, score);
            this.sendMessage(Messages.SCORE_CHANGED);
            break;
          }
        default:
          break;
      }
    }
  }
}
