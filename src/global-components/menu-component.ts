import * as ECSA from '../../libs/pixi-component';
import { Attributes, AppState } from '../constants'
import { AppStateManager } from '../app-state-manager';
import { BaseComponent } from '../components/base-component';

/**
 * Component to display and control main menu
 */
export class MenuComponent extends BaseComponent {
  onInit() {
    super.onInit();
    this.factory.createMainMenu(this.scene);
  }

  onUpdate(delta: number, absolute: number) {
    if (this.input.isKeyPressed(ECSA.Keys.KEY_SPACE)) {
      // start new game
      this.scene.invokeWithDelay(5,
        () => this.scene.getGlobalAttribute<AppStateManager>(Attributes.APP_STATE_MANAGER)
          .switchToState(AppState.PLAY_GAME));
    }
  }
}
