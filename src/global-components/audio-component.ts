import * as ECSA from '../../libs/pixi-component';
import { BaseComponent } from '../components/base-component';
import { Messages, ExplosionType, Assets, Names, LaserType } from '../constants';
import PIXISound from 'pixi-sound';
import { ObjectDamagedMessage } from '../components/destructible';

/**
 * Component that plays sound effects based on received messages
 */
export class AudioComponent extends BaseComponent {
  private currentMusic: Assets = null;

  onInit() {
    super.onInit();
    this.subscribe(Messages.LASER_PROJECTILE_FIRED);
    this.subscribe(Messages.MISSILE_LAUNCHED);
    this.subscribe(Messages.EXPLOSION_TRIGGERED);
    this.subscribe(Messages.OBJECT_DAMAGED);
    this.subscribe(Messages.OBJECT_DESTROYED);
    this.subscribe(Messages.ITEM_COLLECTED);
    this.subscribe(Messages.GAME_START);
    this.subscribe(Messages.GAME_OVER);
  }

  onRemove() {
    this.stopMusic();
    super.onRemove();
  }

  onMessage(msg: ECSA.Message) {
    switch (msg.action) {
      case Messages.LASER_PROJECTILE_FIRED:
        {
          let laserType = msg.data.type as LaserType;
          if (laserType === LaserType.HEAVY || laserType === LaserType.SLOW_HEAVY)
            PIXISound.play(Assets.AUDIO_LASER_HEAVY);
          else
            PIXISound.play(Assets.AUDIO_LASER);
          break;
        }
      case Messages.MISSILE_LAUNCHED:
        PIXISound.play(Assets.AUDIO_MISSILE_LAUNCH);
        break;
      case Messages.EXPLOSION_TRIGGERED:
        {
          let explosionType = msg.data as ExplosionType;
          if (explosionType === ExplosionType.LARGE)
            PIXISound.play(Assets.AUDIO_EXPLOSION_LARGE);
          else
            PIXISound.play(Assets.AUDIO_EXPLOSION_SMALL);
          break;
        }
      case Messages.OBJECT_DAMAGED:
        if ((msg.data as ObjectDamagedMessage).target.name === Names.ASTEROID)
          PIXISound.play(Assets.AUDIO_HIT_ASTEROID);
        else
          PIXISound.play(Assets.AUDIO_HIT);
        break;
      case Messages.OBJECT_DESTROYED:
        if ((msg.data as ObjectDamagedMessage).target.name === Names.ASTEROID)
          PIXISound.play(Assets.AUDIO_HIT_ASTEROID);
        break;
      case Messages.GAME_START:
        this.playMusic(Assets.AUDIO_MUSIC_MAIN);
        break;
      case Messages.ITEM_COLLECTED:
        PIXISound.play(Assets.AUDIO_POWER_UP);
        break;
      case Messages.GAME_OVER:
        PIXISound.play(Assets.AUDIO_GAME_OVER);
        break;
    }
  }

  private playMusic(musicAsset: Assets) {
    this.stopMusic();
    let opts = { loop: true };
    PIXISound.play(musicAsset, opts);
    this.currentMusic = musicAsset;
  }

  private stopMusic() {
    if (this.currentMusic != null) {
      PIXISound.stop(this.currentMusic);
      this.currentMusic = null;
    }
  }
}
