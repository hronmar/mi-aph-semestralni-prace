import * as ECSA from '../../libs/pixi-component';
import { Attributes, Messages, AppState, DEFAULT_GAME_SPEED, PLAYER_SHIP_RESPAWN_TIME, STARTING_PLAYER_LIVES, GAME_OVER_DISPLAY_TIME } from '../constants'
import { BaseComponent } from '../components/base-component';
import { AppStateManager } from '../app-state-manager';

/**
 * Component that orchestrates main logic of the game
 */
export class GameComponent extends BaseComponent {
  onInit() {
    super.onInit();

    // enable zIndex
    this.scene.stage.sortableChildren = true;

    this.initializeGame();
    this.subscribe(Messages.PLAYER_KILLED);
    this.scene.invokeWithDelay(100,
      () => this.sendMessage(Messages.GAME_START));
  }

  onMessage(msg: ECSA.Message) {
    if (msg.action === Messages.PLAYER_KILLED) {
      // decrement player lives
      let lives = this.scene.getGlobalAttribute<number>(Attributes.PLAYER_LIVES);
      lives -= 1;
      this.scene.assignGlobalAttribute(Attributes.PLAYER_LIVES, lives);
      this.sendMessage(Messages.PLAYER_LIVES_CHANGED);
      if (lives <= 0) {
        this.gameOver();
      } else {
        // respawn player
        this.scene.invokeWithDelay(PLAYER_SHIP_RESPAWN_TIME,
          () => this.spawnPlayer(true));
      }
    }
  }

  private initializeGame() {
    this.scene.assignGlobalAttribute(Attributes.GAME_SPEED, DEFAULT_GAME_SPEED);
    this.scene.assignGlobalAttribute(Attributes.PLAYER_LIVES, STARTING_PLAYER_LIVES);
    this.scene.assignGlobalAttribute(Attributes.PLAYER_SCORE, 0);
    this.spawnPlayer();
  }

  private spawnPlayer(respawn: boolean = false) {
    this.factory.createPlayerShip(this.scene, respawn);
  }

  private gameOver() {
    this.sendMessage(Messages.GAME_OVER);
    this.factory.createGameOverAnnouncement(this.scene);
    this.scene.invokeWithDelay(GAME_OVER_DISPLAY_TIME,
      () => {
        let score = this.scene.getGlobalAttribute<number>(Attributes.PLAYER_SCORE);
        this.scene.getGlobalAttribute<AppStateManager>(Attributes.APP_STATE_MANAGER)
          .switchToState(AppState.SCORE_SCREEN, score);
      });
  }
}
