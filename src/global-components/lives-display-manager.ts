import * as ECSA from '../../libs/pixi-component';
import { Attributes, Messages, SCENE_WIDTH, Names, MAX_PLAYER_LIVES } from '../constants'
import { BaseComponent } from '../components/base-component';

/**
 * Component that manages display of player's remaining lives
 */
export class LivesDisplayManager extends BaseComponent {
  private maxLives: number;

  onInit() {
    super.onInit();
    this.maxLives = MAX_PLAYER_LIVES;
    this.createLivesIcons();
    this.subscribe(Messages.PLAYER_LIVES_CHANGED);
    this.updateLivesIcons();
  }

  onMessage(msg: ECSA.Message) {
    if (msg.action === Messages.PLAYER_LIVES_CHANGED) {
      this.updateLivesIcons();
    }
  }

  private createLivesIcons() {
    for (let i = 0; i < this.maxLives; i++) {
      let pos = new ECSA.Vector(SCENE_WIDTH - i * 40 - 25, 25);
      this.factory.createLiveDisplayIcon(this.scene, pos, this.getLiveIconName(i));
    }
  }

  private updateLivesIcons() {
    let lives = this.scene.getGlobalAttribute<number>(Attributes.PLAYER_LIVES);
    for (let i = 0; i < lives; i++) {
      this.scene.findObjectByName(this.getLiveIconName(i)).visible = true;
    }
    for (let i = lives; i < this.maxLives; i++) {
      this.scene.findObjectByName(this.getLiveIconName(i)).visible = false;
    }
  }

  private getLiveIconName(index: number): string {
    return Names.LIVE_ICON + "_" + index;
  }
}
