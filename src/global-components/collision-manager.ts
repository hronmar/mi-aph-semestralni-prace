import * as ECSA from '../../libs/pixi-component';
import { BaseComponent } from '../components/base-component';
import { Flags, Messages, Names } from '../constants';
import { rectanglesIntersect } from '../utils/functions';

/**
 * Objects collision message data
 */
export class CollisionMessage {
  object1: ECSA.Container;
  object2: ECSA.Container;

  constructor(object1: ECSA.Container, object2: ECSA.Container) {
    this.object1 = object1;
    this.object2 = object2;
  }
}

// list of objects that cannot collide with other objects of the same type (name)
const NO_COLLIDE_EACH_OTHER = [Names.ASTEROID, Names.LASER_PROJECTILE];

/**
 * Component that detects collisions between collidable objects
 *  and notifies of them by sending messages
 */
export class CollisionManager extends BaseComponent {
  private collidablesCache: ECSA.Container[];
  private noCollideEachOther: Set<Names>;

  onInit() {
    super.onInit();
    this.subscribe(ECSA.Messages.FLAG_CHANGED, ECSA.Messages.OBJECT_ADDED, ECSA.Messages.OBJECT_REMOVED);
    this.refreshCollidablesCache();
    this.noCollideEachOther = new Set(NO_COLLIDE_EACH_OTHER);
  }

  onUpdate(delta: number, absolute: number) {
    this.detectCollisions();
  }

  onMessage(msg: ECSA.Message) {
    if (msg.action === ECSA.Messages.OBJECT_ADDED
      || msg.action === ECSA.Messages.OBJECT_REMOVED
      || (msg.action === ECSA.Messages.FLAG_CHANGED
        && (<ECSA.FlagChangeMessage>msg.data).flag === Flags.COLLIDABLE)) {
      this.refreshCollidablesCache();
    }
  }

  private detectCollisions() {
    // use quadratic algorithm to check if each pair of objects collides
    for (let i = 0; i < this.collidablesCache.length - 1; i++)
      for (let j = i + 1; j < this.collidablesCache.length; j++) {
        let obj1 = this.collidablesCache[i];
        let obj2 = this.collidablesCache[j];
        if (this.checkCollision(obj1, obj2)) {
          let msgData = new CollisionMessage(obj1, obj2);
          this.sendMessage(Messages.OBJECTS_COLLISION, msgData);
        }
      }
  }

  private checkCollision(object1: ECSA.Container, object2: ECSA.Container): boolean {
    if (object1.name === object2.name
      && this.noCollideEachOther.has(object1.name as Names))
      return false;
    return rectanglesIntersect(object1.getBounds(), object2.getBounds());
  }

  private refreshCollidablesCache() {
    this.collidablesCache = this.scene.findObjectsByFlag(Flags.COLLIDABLE);
  }
}
