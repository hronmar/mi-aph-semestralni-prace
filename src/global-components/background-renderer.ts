import * as ECSA from '../../libs/pixi-component';
import { BaseComponent } from '../components/base-component';
import { checkTime } from '../utils/functions';
import { SCENE_HEIGHT, SCENE_WIDTH } from '../constants';
import Dynamics from '../utils/dynamics';

const STARTING_PARTICLES = 50;
const BATCH_SIZE = 2.5;
const SPAWN_FREQUENCY = 5;
const PARTICLE_MIN_SPEED = 1;
const PARTICLE_MAX_SPEED = 80;
const PARTICLE_MAX_ACCELERATION = 15;

/**
 * Component that displays and animates the background
 */
export class BackgroundRenderer extends BaseComponent {
  private lastSpawnTime = 0;

  onInit() {
    super.onInit();
    this.factory.createBackground(this.scene);
    this.factory.createDustParticleContainer(this.scene);
    for (let i = 0; i < STARTING_PARTICLES; i++)
      this.spawnDustParticle(this.random.uniformInt(0, SCENE_HEIGHT));
  }

  onUpdate(delta: number, absolute: number) {
    if (checkTime(this.lastSpawnTime, absolute, SPAWN_FREQUENCY)) {
      this.lastSpawnTime = absolute;
      for (let i = 0; i < BATCH_SIZE; i++)
        this.spawnDustParticle(0);
    }
  }

  private spawnDustParticle(y: number) {
    let pos = new ECSA.Vector(this.random.uniformInt(0, SCENE_WIDTH), y);
    let dynamics = new Dynamics(new ECSA.Vector(0, this.random.normal(PARTICLE_MIN_SPEED, PARTICLE_MAX_SPEED)),
      new ECSA.Vector(0, this.random.uniform(0, PARTICLE_MAX_ACCELERATION)));
    this.factory.createDustParticle(this.scene, pos, dynamics);
  }
}
