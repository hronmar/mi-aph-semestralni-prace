import * as ECSA from '../../libs/pixi-component';
import { Messages } from '../constants'

/**
 * Input manager that abstracts the input method and optionally provides message notifications
 */
export class InputManager extends ECSA.Component {
  private proxy: ECSA.KeyInputComponent;
  private messageNotifications: boolean;

  constructor(messageNotifications: boolean = false) {
    super();
    this.messageNotifications = messageNotifications;
  }

  onInit() {
    this.initializeInput();
    if (this.messageNotifications) {
      document.addEventListener('keypress', this.onKeyPress, false);
    }
    this.proxy.onInit();
  }

  onRemove() {
    this.proxy.onRemove();
    if (this.messageNotifications) {
      document.removeEventListener('keypress', this.onKeyPress);
    }
  }

  onUpdate(delta: number, absolute: number) {
    this.proxy.onUpdate(delta, absolute);
  }

  isKeyPressed(keyCode: number) {
    return this.proxy.isKeyPressed(keyCode);
  }

  private initializeInput() {
    if (PIXI.utils.isMobile.any) {
      // use virtual gamepad (for mobile)
      this.proxy = new ECSA.VirtualGamepadComponent({
        KEY_LEFT: ECSA.Keys.KEY_LEFT,
        KEY_RIGHT: ECSA.Keys.KEY_RIGHT,
        KEY_UP: ECSA.Keys.KEY_UP,
        KEY_DOWN: ECSA.Keys.KEY_DOWN,
        KEY_X: ECSA.Keys.KEY_SPACE
      });
    } else {
      // use keyboard
      this.proxy = new ECSA.KeyInputComponent();
    }
  }

  private onKeyPress = (evt: KeyboardEvent) => {
    // notify of key press
    this.sendMessage(Messages.INPUT_KEY_PRESSED, evt.keyCode);
  }
}
