import { Rectangle } from "pixi.js";
import * as ECSA from '../../libs/pixi-component';

export const isMobileDevice = () => /(iphone|ipod|ipad|android)/gi.test(navigator.userAgent);

export const isPdfPrint = () => window.location.search.match(/print-pdf/gi);

/**
 * Resizes container to a fixed resolution
 */
export const resizeContainer = (container: HTMLElement, virtualWidth: number, virtualHeight: number) => {
  let scale: number;
  let isVertical = false;
  if (window.innerWidth / window.innerHeight > virtualWidth / virtualHeight) {
    scale = window.innerHeight / virtualHeight;
  } else {
    scale = window.innerWidth / virtualWidth;
    isVertical = true;
  }

  // On some mobile devices '100vh' is taller than the visible
  // viewport which leads to part of the presentation being
  let hideAddressBar = () => setTimeout(() => { window.scrollTo(0, 1); }, 10);

  if (isMobileDevice()) {
    document.documentElement.style.setProperty('--vh', (window.innerHeight * 0.01) + 'px');
    // Events that should trigger the address bar to hide
    window.addEventListener('load', hideAddressBar, false);
    window.addEventListener('orientationchange', hideAddressBar, false);
  }

  let transform = `scale(${scale})`;
  let topPos = isVertical ? window.innerHeight / 2 - virtualHeight / 2 : ((scale - 1) * virtualHeight / 2);
  container.style.setProperty('position', 'absolute');
  container.style.setProperty('MozTransform', transform);
  container.style.setProperty('transform', transform);
  container.style.setProperty('WebkitTransform', transform);
  container.style.setProperty('top', topPos + 'px');
  container.style.setProperty('left', ((scale - 1) * virtualWidth / 2 + (window.innerWidth - virtualWidth * scale) / 2) + 'px');
};

/**
 * Randomly sorts an array
 */
export const shuffle = (arr: Array<any>) => {
  for (let i = arr.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [arr[i], arr[j]] = [arr[j], arr[i]];
  }
  return arr;
};

/**
 * Returns true if given time has already reached or exceeded certain period
 */
export const checkTime = (lastTime: number, time: number, frequency: number) => {
  return (time - lastTime) > 1000 / frequency;
};

/**
 * Restrict number into given interval [min, max] inclusive
 */
export const ensureRange = (x: number, min: number, max: number) => {
  return Math.max(min, Math.min(x, max));
};

/**
 * Checks whether two rectangles intersect
 */
export const rectanglesIntersect = (rect1: Rectangle, rect2: Rectangle) => {
  return (rect1 === rect2
    || (rect1.left <= rect2.right
      && rect1.right >= rect2.left
      && rect1.top <= rect2.bottom
      && rect1.bottom >= rect2.top));
};

/**
 * Rotates vector by given angle in radians and returns the result
 */
export const rotateVector = (vector: ECSA.Vector, angle: number) => {
  let cs = Math.cos(angle);
  let sn = Math.sin(angle);
  let newX = cs * vector.x - sn * vector.y;
  let newY = sn * vector.x + cs * vector.y;
  return new ECSA.Vector(newX, newY);
};

/**
 * Converts ECSA.Vector to PIXI.Point
 */
export const vectorToPoint = (vector: ECSA.Vector) => {
  return new PIXI.Point(vector.x, vector.y);
}

/**
 * Converts PIXI.Point to ECSA.Vector
 */
export const pointToVector = (point: PIXI.IPoint) => {
  return new ECSA.Vector(point.x, point.y);
}
