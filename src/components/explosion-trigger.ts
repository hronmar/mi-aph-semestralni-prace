import * as ECSA from '../../libs/pixi-component';
import { DestructionTriggerBase } from "./destruction-trigger-base";
import { Messages, ExplosionType } from "../constants";

/**
 * Component that makes an explosion when its owner is destroyed
 */
export class ExplosionTrigger extends DestructionTriggerBase {
  private explosionType: ExplosionType;

  constructor(explosionType: ExplosionType = ExplosionType.LARGE) {
    super();
    this.explosionType = explosionType;
  }

  execute() {
    let pos = new ECSA.Vector(this.owner.x, this.owner.y);
    this.factory.createExplosion(this.scene, pos, this.explosionType);
    this.sendMessage(Messages.EXPLOSION_TRIGGERED, this.explosionType);
  }
}
