import * as ECSA from '../../libs/pixi-component';
import { SteeringMath } from "../../libs/pixi-math";
import { BaseComponent } from './base-component';
import Dynamics from '../utils/dynamics';
import { pointToVector } from '../utils/functions';
import { Attributes } from '../constants';

/**
 * Base class for all steering components. 
 * Object that is given any steering component must have dynamics component.
 */
export abstract class SteeringComponent extends BaseComponent {
  protected dynamics: Dynamics;
  protected steeringMath: SteeringMath;
  protected maxAcceleration: number;
  protected maxVelocity: number;
  protected handleRotation: boolean;
  private active: boolean;

  constructor(maxAcceleration: number, maxVelocity: number, handleRotation: boolean) {
    super();
    this.steeringMath = new SteeringMath();
    this.maxAcceleration = maxAcceleration;
    this.maxVelocity = maxVelocity;
    this.handleRotation = handleRotation;
    this.active = true;
  }

  onInit() {
    super.onInit();
    this.dynamics = this.owner.getAttribute(Attributes.DYNAMICS);

    if (this.dynamics == null) {
      // ensure object has dynamics component
      throw new Error('Object must have dynamics component for steering to work');
    }

    if (this.dynamics.velocity.magnitude() === 0) {
      // ensure non-zero initial velocity
      this.dynamics.velocity = new ECSA.Vector(this.random.uniform(5, 10), this.random.uniform(5, 10));
    }
  }

  onUpdate(delta: number, absolute: number) {
    if (!this.active) {
      return;
    }

    let force = this.calculateForce(delta);
    if (force == null) {
      return; // algorithm has finished
    }

    // update acceleration and velocity
    this.dynamics.acceleration = force;
    this.dynamics.acceleration.limit(this.maxAcceleration);
    this.dynamics.velocity.limit(this.maxVelocity);

    if (this.handleRotation) {
      // change rotation based on the velocity
      let currentAngle = Math.atan2(this.dynamics.velocity.y, this.dynamics.velocity.x) + Math.PI / 2;
      this.owner.rotation = currentAngle;
    }
  }

  setActive(active: boolean) {
    this.active = active;
  }

  isActive() {
    return this.active;
  }

  abstract calculateForce(delta: number): ECSA.Vector;
}

/**
 * Component for composition of multiple steering behaviours.
 * NOTE: Child steering components must be added to object manually!
 */
export class CompoundSteeringComponent extends SteeringComponent {
  protected steeringComponents: SteeringComponent[];
  protected componentWeights: number[];

  constructor(maxAcceleration: number, maxVelocity: number, handleRotation: boolean,
    components: SteeringComponent[] = []) {
    super(maxAcceleration, maxVelocity, handleRotation);
    this.steeringComponents = components;
    this.componentWeights = new Array<number>(this.steeringComponents.length);
    this.componentWeights = this.componentWeights.fill(1);
  }

  onInit() {
    super.onInit();

    // set child components as inactive, steering force will be computed by this component
    for (let comp of this.steeringComponents)
      comp.setActive(false);
  }

  addSteeringComponent(component: SteeringComponent, weight: number = 1) {
    component.setActive(false);
    this.steeringComponents.push(component);
    this.componentWeights.push(weight);
  }

  calculateForce(delta: number): ECSA.Vector {
    let jointForce = new ECSA.Vector(0, 0);

    for (let i = 0; i < this.steeringComponents.length; i++) {
      let steeringComp = this.steeringComponents[i];
      let weight = this.componentWeights[i];
      let force = steeringComp.calculateForce(delta);
      if (force != null)
        jointForce = jointForce.add(force.multiply(weight));
    }

    return jointForce;
  }
}

/**
 * Component for wander steering
 */
export class WanderSteeringComponent extends SteeringComponent {
  protected wanderTarget = new ECSA.Vector(0, 0);
  protected wanderDistance: number;
  protected wanderRadius: number;
  protected wanderJittering: number;

  constructor(maxAcceleration: number, maxVelocity: number, handleRotation: boolean,
    wanderDistance: number, wanderRadius: number, wanderJittering: number) {
    super(maxAcceleration, maxVelocity, handleRotation);
    this.wanderDistance = wanderDistance;
    this.wanderRadius = wanderRadius;
    this.wanderJittering = wanderJittering;
  }

  calculateForce(delta: number): ECSA.Vector {
    let force = this.steeringMath.wander(this.dynamics.velocity, this.wanderTarget, this.wanderRadius, this.wanderDistance, this.wanderJittering, delta);
    this.wanderTarget = force[1];
    return force[0];
  }
}

/**
 * Component for seek to given point
 */
export class SeekSteeringComponent extends SteeringComponent {
  targetPosition: ECSA.Vector;
  continueTrajectory: boolean;
  continueTrajectoryRadius: number = 70;

  constructor(maxAcceleration: number, maxVelocity: number, handleRotation: boolean,
    targetPosition: ECSA.Vector, continueTrajectory: boolean = false) {
    super(maxAcceleration, maxVelocity, handleRotation);
    this.targetPosition = targetPosition;
    this.continueTrajectory = continueTrajectory;
  }

  calculateForce(delta: number): ECSA.Vector {
    let ownerPosition = pointToVector(this.owner.position);
    let result = this.steeringMath.seek(this.targetPosition, ownerPosition, this.dynamics.velocity, this.maxVelocity, null);
    let dist = this.targetPosition.distance(ownerPosition);
    if (dist < 1) {
      if (this.isActive()) {
        this.finish();
      }
      return null;
    }
    if (this.continueTrajectory && dist < this.continueTrajectoryRadius) {
      // move target point ahead of us to prevent circling around target point (continue trajectory)
      this.targetPosition = this.targetPosition
        .add(this.dynamics.velocity.normalize().multiply(this.continueTrajectoryRadius));
    }
    return result;
  }
}

/**
 * Component for pursuit of given object
 */
export class PursuitSteeringComponent extends SteeringComponent {
  target: ECSA.Container;

  constructor(maxAcceleration: number, maxVelocity: number, handleRotation: boolean,
    target: ECSA.Container) {
    super(maxAcceleration, maxVelocity, handleRotation);
    this.target = target;
  }

  calculateForce(delta: number): ECSA.Vector {
    let targetPos = new ECSA.Vector(this.target.position.x, this.target.position.y);
    let ownerPos = new ECSA.Vector(this.owner.position.x, this.owner.position.y);
    let targetVelocity = this.target.getAttribute<Dynamics>(Attributes.DYNAMICS).velocity;
    return this.steeringMath.pursuit(targetPos, ownerPos, 300, this.dynamics.velocity, targetVelocity);
  }
}
