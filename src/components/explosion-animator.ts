import { BaseComponent } from './base-component';

const EXPLOSION_ANIMATION_PERIOD = 150;

/**
 * Component for explosion animation
 */
export class ExplosionAnimator extends BaseComponent {
  private animation: any;
  private currentFrame: number;
  private accTime: number;
  private period: number;

  constructor(animation: any) {
    super();
    this.animation = animation;
    this.accTime = 0;
    this.period = EXPLOSION_ANIMATION_PERIOD;
  }

  onInit() {
    super.onInit();
    this.currentFrame = 0;
    this.updateSprite();
  }

  onUpdate(delta: number, absolute: number) {
    this.accTime += delta;
    if (this.accTime >= this.period) {
      this.accTime = 0;
      if (this.currentFrame++ >= this.animation.length) {
        // animation finished, remove object from scene
        this.removeObject(this.owner);
      } else {
        this.updateSprite();
      }
    }
  }

  private updateSprite() {
    this.owner.asSprite().texture = this.animation[this.currentFrame];
  }
}
