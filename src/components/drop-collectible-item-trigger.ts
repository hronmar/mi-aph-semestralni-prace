import * as ECSA from '../../libs/pixi-component';
import { DestructionTriggerBase } from "./destruction-trigger-base";
import { CollectibleItem } from "./collectible";
import { pointToVector } from "../utils/functions";
import Dynamics from "../utils/dynamics";
import { ITEMS_SPEED } from '../constants';

/**
 * Component that drops collectible item when its owner (enemy) is destroyed
 */
export class DropCollectibleItemTrigger extends DestructionTriggerBase {
  private item: CollectibleItem;

  constructor(item: CollectibleItem) {
    super();
    this.item = item;
  }

  execute() {
    let dynamics = new Dynamics(new ECSA.Vector(0, ITEMS_SPEED));
    this.factory.createCollectibleItem(this.scene, pointToVector(this.owner.position), dynamics,
      this.item);
  }
}
