import { DestructionTriggerBase } from "./destruction-trigger-base";
import { Messages } from "../constants";

/**
 * Component that announces player death when its owner (player) is destroyed
 */
export class PlayerDeathTrigger extends DestructionTriggerBase {
  execute() {
    this.sendMessage(Messages.PLAYER_KILLED);
  }
}
