import * as ECSA from '../../libs/pixi-component';
import { Attributes, DEFAULT_PLAYER_SHIP_SPEED, PLAYER_SHOT_COOLDOWN, LaserSpeed, LaserType } from '../constants'
import { BaseComponent } from './base-component';
import Dynamics from '../utils/dynamics';
import { pointToVector } from '../utils/functions';

enum MoveDirection {
  LEFT, RIGHT, UP, DOWN
}

/**
 * Component controlling movement and actions of player's ship
 */
class PlayerShipController extends BaseComponent {
  private lastShot: number;
  private lastPosition: ECSA.Vector;

  onInit() {
    super.onInit();
    this.lastShot = 0;
    this.owner.assignAttribute(Attributes.PLAYER_SHIP_SPEED, DEFAULT_PLAYER_SHIP_SPEED);
    this.owner.assignAttribute(Attributes.DYNAMICS, new Dynamics());
    this.lastPosition = pointToVector(this.owner.position);
  }

  onUpdate(delta: number, absolute: number) {
    // compute current velocity based on player's ship movement, used for steering
    let currentPos = pointToVector(this.owner.position);
    let velocity = currentPos.subtract(this.lastPosition);
    this.owner.assignAttribute(Attributes.DYNAMICS, new Dynamics(velocity));
    this.lastPosition = currentPos;
  }

  move(direction: MoveDirection, delta: number) {
    let gameSpeed: number = this.scene.getGlobalAttribute(Attributes.GAME_SPEED);
    let shipSpeed: number = this.owner.getAttribute(Attributes.PLAYER_SHIP_SPEED);
    let movement = delta * shipSpeed * gameSpeed;
    switch (direction) {
      case MoveDirection.LEFT:
        this.owner.position.x -= movement;
        break;
      case MoveDirection.RIGHT:
        this.owner.position.x += movement;
        break;
      case MoveDirection.UP:
        this.owner.position.y -= movement;
        break;
      case MoveDirection.DOWN:
        this.owner.position.y += movement;
        break;
    }
  }

  shoot(absolute: number) {
    if (absolute - this.lastShot >= PLAYER_SHOT_COOLDOWN) {
      this.lastShot = absolute;
      let position = new ECSA.Vector(this.owner.position.x, this.owner.position.y - 60);
      let dynamics = new Dynamics(new ECSA.Vector(0, -LaserSpeed(LaserType.NORMAL)));
      this.factory.createLaserProjectile(this.scene, position, dynamics, this.owner);
    }
  }
}

/**
 * Controller for player's ship using user input
 */
export class PlayerShipInputController extends PlayerShipController {
  onInit() {
    super.onInit();
  }

  onUpdate(delta: number, absolute: number) {
    if (this.input.isKeyPressed(ECSA.Keys.KEY_LEFT)
      || this.input.isKeyPressed(ECSA.Keys.KEY_A)) {
      this.move(MoveDirection.LEFT, delta);
    }

    if (this.input.isKeyPressed(ECSA.Keys.KEY_RIGHT)
      || this.input.isKeyPressed(ECSA.Keys.KEY_D)) {
      this.move(MoveDirection.RIGHT, delta);
    }

    if (this.input.isKeyPressed(ECSA.Keys.KEY_UP)
      || this.input.isKeyPressed(ECSA.Keys.KEY_W)) {
      this.move(MoveDirection.UP, delta);
    }

    if (this.input.isKeyPressed(ECSA.Keys.KEY_DOWN)
      || this.input.isKeyPressed(ECSA.Keys.KEY_S)) {
      this.move(MoveDirection.DOWN, delta);
    }

    if (this.input.isKeyPressed(ECSA.Keys.KEY_SPACE)
      || this.input.isKeyPressed(ECSA.Keys.KEY_F)) {
      this.shoot(absolute);
    }

    super.onUpdate(delta, absolute);
  }
}
