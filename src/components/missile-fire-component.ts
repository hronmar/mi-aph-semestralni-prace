import * as ECSA from '../../libs/pixi-component';
import { pointToVector } from '../utils/functions';
import Dynamics from '../utils/dynamics';
import { EnemyFireComponentBase } from './enemy-fire-component-base';
import { MissileType, MISSILE_SPEED, SCENE_HEIGHT } from '../constants';

/**
 * Components that periodically fires laser projectiles from enemy at player's ship
 */
export class MissileFireComponent extends EnemyFireComponentBase {
  private missileType: MissileType;

  constructor(shootFrequency: number, missileType: MissileType) {
    super(shootFrequency);
    this.missileType = missileType;
  }

  fire(target: ECSA.Container) {
    let targetPos = pointToVector(target.position);
    let velocity: ECSA.Vector;
    let position: ECSA.Vector;
    if (targetPos.y > this.owner.y && this.owner.y <= SCENE_HEIGHT / 2) {
      // target is ahead of us, launch missile forward
      velocity = new ECSA.Vector(0, MISSILE_SPEED);
      position = new ECSA.Vector(this.owner.x, this.owner.y + this.owner.height);
    } else {
      // target is behind us, launch missile from broadside
      let dir = targetPos.x < this.owner.x ? -1 : 1;
      velocity = new ECSA.Vector(dir * MISSILE_SPEED, 0);
      position = new ECSA.Vector(this.owner.x + dir * this.owner.width, this.owner.y);
    }
    this.factory.createMissile(this.scene, position, new Dynamics(velocity), target, this.missileType);
  }
}
