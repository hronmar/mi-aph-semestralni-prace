import { DestructionTriggerBase } from "./destruction-trigger-base";
import { Messages, Attributes } from "../constants";

/**
 * Component that gives score to player when its owner (enemy) is destroyed
 */
export class GiveScoreTrigger extends DestructionTriggerBase {
  private scorePoints: number;

  constructor(scorePoints: number) {
    super();
    this.scorePoints = scorePoints;
  }

  execute() {
    let score = this.scene.getGlobalAttribute<number>(Attributes.PLAYER_SCORE);
    score += this.scorePoints;
    this.scene.assignGlobalAttribute(Attributes.PLAYER_SCORE, score);
    this.sendMessage(Messages.SCORE_CHANGED);
  }
}
