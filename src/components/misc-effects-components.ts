import { BaseComponent } from "./base-component";

/**
 * Component that periodically hides and shows its owner (blinking effect)
 */
export class BlinkComponent extends BaseComponent {
  private visiblePeriod: number;
  private hiddenPeriod: number;
  private lastBlink: number;

  constructor(visiblePeriod: number, hiddenPeriod: number) {
    super();
    this.visiblePeriod = visiblePeriod;
    this.hiddenPeriod = hiddenPeriod;
    this.lastBlink = 0;
  }

  onUpdate(delta: number, absolute: number) {
    if (this.owner.visible) {
      if (absolute - this.lastBlink >= this.visiblePeriod) {
        this.owner.visible = false;
        this.lastBlink = absolute;
      }
    } else {
      if (absolute - this.lastBlink >= this.hiddenPeriod) {
        this.owner.visible = true;
        this.lastBlink = absolute;
      }
    }
  }
}

/**
 * Component that rotates its owner by given speed
 */
export class RotationComponent extends BaseComponent {
  private rotationSpeed: number;

  constructor(rotationSpeed: number) {
    super();
    this.rotationSpeed = rotationSpeed;
  }

  onUpdate(delta: number, absolute: number) {
    this.owner.rotation += delta * this.rotationSpeed;
  }
}

/**
 * Component that sets its owner's transparency to given value and then resets it when it is removed
 */
export class TransparencyComponent extends BaseComponent {
  private transparency: number;
  private originalTransparency: number;

  constructor(transparency: number) {
    super();
    this.transparency = transparency;
  }

  onInit() {
    super.onInit();
    this.originalTransparency = this.owner.alpha;
    this.owner.alpha = this.transparency;
  }

  onRemove() {
    this.owner.alpha = this.originalTransparency;
    super.onRemove();
  }
}
