import * as ECSA from '../../libs/pixi-component';
import { BaseComponent } from './base-component';
import { Flags, Messages } from '../constants';

/**
 * Component that grants its owner shield (temporary indestructibility)
 */
export class ShieldComponent extends BaseComponent {
  private duration: number;

  constructor(duration: number) {
    super();
    this.duration = duration;
  }

  onInit() {
    super.onInit();
    this.owner.resetFlag(Flags.COLLIDABLE);
    this.owner.alpha = 0.5;
    this.removeWhenFinished = true;
    this.subscribe(Messages.LASER_PROJECTILE_FIRED);
  }

  onUpdate(delta: number, absolute: number) {
    this.duration -= delta;
    if (this.duration <= 0) {
      this.finish();
    }
  }

  onMessage(msg: ECSA.Message) {
    if (msg.action === Messages.LASER_PROJECTILE_FIRED
      && (msg.data.source as ECSA.Container) === this.owner) {
      // protection ends when owner attacks
      this.finish();
    }
  }

  onFinish() {
    this.owner.setFlag(Flags.COLLIDABLE);
    this.owner.alpha = 1;
    super.onFinish();
  }
}
