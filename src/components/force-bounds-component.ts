import { BaseComponent } from './base-component';
import { SCENE_WIDTH, SCENE_HEIGHT } from '../constants';
import { ensureRange } from '../utils/functions';

/**
 * Component that forces its owner to stay within level (scene) bounds
 */
export class ForceBoundsComponent extends BaseComponent {
  private boundaries: PIXI.Rectangle;

  onInit() {
    super.onInit();

    // precompute boundaries where the owner can go
    let w = this.owner.width;
    let h = this.owner.height;
    this.boundaries = new PIXI.Rectangle(w / 2, h / 2, SCENE_WIDTH - w, SCENE_HEIGHT - h);
  }

  onUpdate() {
    // ensure boundaries
    this.owner.position.x = ensureRange(this.owner.position.x, this.boundaries.left, this.boundaries.right);
    this.owner.position.y = ensureRange(this.owner.position.y, this.boundaries.top, this.boundaries.bottom);
  }
}
