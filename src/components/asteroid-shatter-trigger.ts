import { DestructionTriggerBase } from "./destruction-trigger-base";
import { AsteroidSize, AsteroidType, Attributes } from "../constants";
import Dynamics from "../utils/dynamics";
import { rotateVector, pointToVector } from '../utils/functions';

/**
 * Component that shatters large asteroid on smaller asteroids when it is destroyed
 */
export class AsteroidShatterTrigger extends DestructionTriggerBase {
    private asteroidSize: AsteroidSize;
    private asteroidType: AsteroidType;

    constructor(asteroidSize: AsteroidSize, asteroidType: AsteroidType) {
        super();
        this.asteroidSize = asteroidSize;
        this.asteroidType = asteroidType;
    }

    execute() {
        let smallerSize: AsteroidSize;
        if (this.asteroidSize === AsteroidSize.LARGE) {
            smallerSize = AsteroidSize.MEDIUM;
        } else if (this.asteroidSize === AsteroidSize.MEDIUM) {
            smallerSize = AsteroidSize.SMALL;
        } else {
            // small asteroids do not shatter further
            return;
        }

        //make fragments
        let numFragments = this.random.uniformInt(2, 3);
        for (let i = 0; i < numFragments; i++)
            this.makeAsteroidFragment(smallerSize, -3 * Math.PI / 4, 3 * Math.PI / 4);
    }

    private makeAsteroidFragment(smallerSize: AsteroidSize, rotationMin: number, rotationMax: number) {
        let baseVelocity = this.owner.getAttribute<Dynamics>(Attributes.DYNAMICS).velocity;
        let rotation = this.random.normal(rotationMin, rotationMax);
        let velocity = rotateVector(baseVelocity, rotation);
        velocity.multiply(this.random.normal(0.9, 1.1));
        let dynamics = new Dynamics(velocity);
        let position = pointToVector(this.owner.position);
        let size = smallerSize;

        if (this.asteroidSize === AsteroidSize.LARGE) {
            //with some probability generate asteroid of smaller size
            if (this.random.uniform(0, 100) >= 70)
                size = AsteroidSize.SMALL;
        }

        this.factory.createAsteroid(this.scene, position, dynamics, this.random.uniform(0, 2 * Math.PI),
            size, this.asteroidType);
    }
}
