import * as ECSA from '../../libs/pixi-component';
import { BaseComponent } from './base-component';
import { Attributes, Flags, Messages } from '../constants';

/**
 * Object destruction message data
 */
export class ObjectDestroyedMessage {
  target: ECSA.Container;

  constructor(target: ECSA.Container) {
    this.target = target;
  }
}

/**
 * Object damaged message data
 */
export class ObjectDamagedMessage {
  target: ECSA.Container;
  damage: number;

  constructor(target: ECSA.Container, damage: number) {
    this.target = target;
    this.damage = damage;
  }
}

/**
 * Component for destructible objects
 */
export class Destructible extends BaseComponent {
  private hitPoints: number;

  constructor(hitPoints: number) {
    super();
    this.hitPoints = hitPoints;
  }

  onInit() {
    super.onInit();
    this.owner.setFlag(Flags.DESTRUCTIBLE);
    this.owner.assignAttribute(Attributes.HITPOINTS, this.hitPoints);
  }

  onRemove() {
    this.owner.resetFlag(Flags.DESTRUCTIBLE);
    super.onRemove();
  }

  impactBy(impactor: ECSA.Container) {
    let currentHP = this.owner.getAttribute<number>(Attributes.HITPOINTS);
    let damage = impactor.getAttribute<number>(Attributes.IMPACT_DAMAGE);
    currentHP -= damage;
    this.owner.assignAttribute(Attributes.HITPOINTS, currentHP);
    if (currentHP <= 0) {
      // notify of destruction
      this.sendMessage(Messages.OBJECT_DESTROYED, new ObjectDestroyedMessage(this.owner));
    } else {
      // notify of damage
      this.sendMessage(Messages.OBJECT_DAMAGED, new ObjectDamagedMessage(this.owner, damage));
    }
  }
}
