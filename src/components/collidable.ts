import { BaseComponent } from './base-component';
import { Attributes, Flags } from '../constants';

/**
 * Component for objects that can collide with other objects
 */
export class Collidable extends BaseComponent {
  private impactDamage: number;

  constructor(impactDamage: number = 0) {
    super();
    this.impactDamage = impactDamage;
  }

  onInit() {
    super.onInit();
    this.owner.setFlag(Flags.COLLIDABLE);
    this.owner.assignAttribute(Attributes.IMPACT_DAMAGE, this.impactDamage);
  }

  onRemove() {
    this.owner.resetFlag(Flags.COLLIDABLE);
    super.onRemove();
  }
}
