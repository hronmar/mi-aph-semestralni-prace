import * as ECSA from '../../libs/pixi-component';
import { Attributes } from '../constants'
import { Factory } from '../factory';
import { InputManager } from '../global-components/input-manager';
import { Random } from '../../libs/pixi-math';

/**
 * Base class for components
 */
export class BaseComponent extends ECSA.Component {
  protected factory: Factory;
  protected input: InputManager;
  protected random: Random;

  onInit() {
    this.factory = this.scene.getGlobalAttribute(Attributes.FACTORY);
    this.input = this.scene.getGlobalAttribute(Attributes.INPUT_MANAGER);
    this.random = this.scene.getGlobalAttribute(Attributes.RANDOM_GENERATOR);
  }

  protected removeObject(object: ECSA.Container) {
    if (object.parent != null) {
      object.remove();
    }
  }
}
