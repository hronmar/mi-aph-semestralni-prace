import { BaseComponent } from './base-component';
import { Attributes, Flags, Messages } from '../constants';

/**
 * Base class for components that trigger an action when object is destroyed
 */
export abstract class DestructionTriggerBase extends BaseComponent {
  onInit() {
    super.onInit();
    if (!this.owner.hasFlag(Flags.DESTRUCTION_TRIGGER)) {
      this.owner.setFlag(Flags.DESTRUCTION_TRIGGER);
      this.owner.assignAttribute(Attributes.DESTRUCTION_TRIGGERS, new Set<DestructionTriggerBase>());
    }

    let triggers = this.owner.getAttribute<Set<DestructionTriggerBase>>(Attributes.DESTRUCTION_TRIGGERS);
    triggers.add(this);
  }

  onRemove() {
    let triggers = this.owner.getAttribute<Set<DestructionTriggerBase>>(Attributes.DESTRUCTION_TRIGGERS);
    triggers.delete(this);
    if (triggers.size === 0) {
      // this was the last trigger
      this.owner.resetFlag(Flags.DESTRUCTION_TRIGGER);
    }
    super.onRemove();
  }

  /**
   * Execute this trigger, called by destruction manager upon object destruction
   */
  abstract execute(): void;
}
