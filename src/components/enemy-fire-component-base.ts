import * as ECSA from '../../libs/pixi-component';
import { BaseComponent } from './base-component';
import { Names, SCENE_WIDTH, SCENE_HEIGHT } from '../constants';
import { checkTime } from '../utils/functions';

/**
 * Base class for components to periodically launch projectiles from enemy at player's ship
 */
export abstract class EnemyFireComponentBase extends BaseComponent {
  private shootFrequency: number;
  private lastShotTime: number;
  private visibleBounds: PIXI.Rectangle;

  constructor(shootFrequency: number) {
    super();
    this.shootFrequency = shootFrequency;
    this.lastShotTime = 0;
  }

  onInit() {
    super.onInit();
    let w = this.owner.width;
    let h = this.owner.height;
    this.visibleBounds = new PIXI.Rectangle(w / 2, h / 2, SCENE_WIDTH - w, SCENE_HEIGHT - h);
  }

  onUpdate(delta: number, absolute: number) {
    if (checkTime(this.lastShotTime, absolute, this.shootFrequency)) {
      this.lastShotTime = absolute;
      this.shootAtPlayer();
    }
  }

  private shootAtPlayer() {
    if (!this.visibleBounds.contains(this.owner.position.x, this.owner.position.y)) {
      // do not shoot if not inside visible area
      return;
    }
    let player = this.scene.findObjectByName(Names.PLAYER_SHIP);
    if (player == null) {
      // player ship is already destroyed
      return;
    }
    // 
    this.fire(player);
  }

  abstract fire(target: ECSA.Container): void;
}
