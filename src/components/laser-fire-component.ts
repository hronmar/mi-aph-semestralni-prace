import * as ECSA from '../../libs/pixi-component';
import { LaserColor, LaserType, LaserSpeed } from '../constants';
import { pointToVector } from '../utils/functions';
import Dynamics from '../utils/dynamics';
import { EnemyFireComponentBase } from './enemy-fire-component-base';

/**
 * Components that periodically fires laser projectiles from enemy at player's ship
 */
export class LaserFireComponent extends EnemyFireComponentBase {
  private shootAngle: number;
  private laserType: LaserType;
  private laserColor: LaserColor;

  constructor(shootFrequency: number, laserType: LaserType, laserColor: LaserColor,
    shootAngle?: number) {
    super(shootFrequency);
    this.shootAngle = shootAngle;
    this.laserType = laserType;
    this.laserColor = laserColor;
  }

  fire(target: ECSA.Container) {
    let targetPos = pointToVector(target.position);
    let startPos = pointToVector(this.owner.position);
    let velocity = targetPos.subtract(startPos).normalize();
    startPos = startPos.add(velocity.multiply(100));
    let dynamics = new Dynamics(velocity.multiply(LaserSpeed(this.laserType)));
    let rotation = Math.atan2(dynamics.velocity.y, dynamics.velocity.x) + Math.PI / 2;
    if (this.shootAngle != null && Math.abs(rotation - this.owner.rotation) < this.shootAngle) {
      // do not shoot if angle towards target is greater than shooting angle
      return;
    }
    this.factory.createLaserProjectile(this.scene, startPos, dynamics, this.owner,
      rotation, this.laserColor, this.laserType);
  }
}
