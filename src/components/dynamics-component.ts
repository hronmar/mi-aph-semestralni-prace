import * as ECSA from '../../libs/pixi-component';
import Dynamics from '../utils/dynamics';
import { Attributes } from '../constants';

/**
 * Component that updates position of an object
 */
export default class DynamicsComponent extends ECSA.Component {
  protected dynamics: Dynamics;
  protected attrName: string;

  constructor(attrName: string) {
    super();
    this.attrName = attrName;
  }

  onInit() {
    this.dynamics = this.owner.getAttribute(this.attrName);
    if (this.dynamics == null) {
      // add an initial one
      this.dynamics = new Dynamics();
      this.owner.assignAttribute(this.attrName, this.dynamics);
    }
  }

  onUpdate(delta: number, absolute: number) {
    let gameSpeed: number = this.scene.getGlobalAttribute(Attributes.GAME_SPEED);
    this.dynamics.applyVelocity(delta, gameSpeed);

    // calculate delta position
    let deltaPos = this.dynamics.calcPositionChange(delta, gameSpeed);
    this.owner.pixiObj.position.x += deltaPos.x;
    this.owner.pixiObj.position.y += deltaPos.y;
  }
}
