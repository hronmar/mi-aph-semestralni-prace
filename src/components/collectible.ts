import { BaseComponent } from './base-component';
import { Attributes, Flags } from '../constants';

/**
 * Types of collectible items
 */
export enum CollectibleItem {
  SHIELD_POWER_UP = 'shield_power_up',
  BONUS_LIVE = 'bonus_live',
  BONUS_SCORE = 'bonus_score'
}

/**
 * Component for objects that may be collected by player (eg. power-ups)
 * Note: object should also have Collidable component so player is able to collect it
 */
export class Collectible extends BaseComponent {
  private item: CollectibleItem;

  constructor(item: CollectibleItem) {
    super();
    this.item = item;
  }

  onInit() {
    super.onInit();
    this.owner.setFlag(Flags.COLLECTIBLE);
    this.owner.assignAttribute(Attributes.COLLECTIBLE_ITEM, this.item);
  }

  onRemove() {
    this.owner.resetFlag(Flags.COLLECTIBLE);
    super.onRemove();
  }
}
