/**
 * Global constants and enums
 */

export const SCENE_WIDTH = 800;
export const SCENE_HEIGHT = 600;

export enum Messages {
  OBJECTS_COLLISION = 'objects_collision',
  OBJECT_DESTROYED = 'object_destroyed',
  PLAYER_KILLED = 'player_killed',
  PLAYER_LIVES_CHANGED = 'player_lives_changed',
  LASER_PROJECTILE_FIRED = 'laser_projectile_fired',
  MISSILE_LAUNCHED = 'missile_launched',
  EXPLOSION_TRIGGERED = 'explosion_triggered',
  OBJECT_DAMAGED = 'object_damaged',
  ITEM_COLLECTED = 'item_collected',
  GAME_START = 'game_start',
  GAME_OVER = 'game_over',
  INPUT_KEY_PRESSED = 'input_key_pressed',
  SCORE_CHANGED = 'score_changed'
}

export enum Attributes {
  HITPOINTS = 'hitpoints',
  IMPACT_DAMAGE = 'impact_damage',
  DYNAMICS = 'dynamics',
  DESTRUCTION_TRIGGERS = 'destruction_triggers',
  COLLECTIBLE_ITEM = 'collectible_item',
  PLAYER_LIVES = 'player_lives',
  PLAYER_SHIP_SPEED = 'player_ship_speed',
  GAME_SPEED = 'game_speed',
  PLAYER_SCORE = 'player_score',
  FACTORY = 'factory',
  INPUT_MANAGER = 'input_manager',
  APP_STATE_MANAGER = 'app_state_manager',
  RANDOM_GENERATOR = 'random_generator'
}

export enum Flags {
  COLLIDABLE = 1,
  DESTRUCTIBLE = 2,
  COLLECTIBLE = 3,
  DESTRUCTION_TRIGGER = 4,
  REMOVE_OUTSIDE_SCENE = 5
}

export enum Assets {
  GFX_TEXTURES = 'gfx_textures',
  JSON_TEXTURE_ATLAS = 'json_texture_atlas',
  SPRITE_EXPLOSION = 'sprite_explosion',
  AUDIO_LASER = 'audio_laser',
  AUDIO_LASER_HEAVY = 'audio_laser_heavy',
  AUDIO_EXPLOSION_LARGE = 'audio_explosion_large',
  AUDIO_EXPLOSION_SMALL = 'audio_explosion_small',
  AUDIO_POWER_UP = 'audio_power_up',
  AUDIO_HIT = 'audio_hit',
  AUDIO_HIT_ASTEROID = 'audio_hit_asteroid',
  AUDIO_MISSILE_LAUNCH = 'audio_missile_launch',
  AUDIO_GAME_OVER = 'audio_game_over',
  AUDIO_MUSIC_MAIN = 'audio_music_main',
  GFX_BACKGROUND_STARS = 'gfx_background_stars',
  GFX_BACKGROUND_NEBULA = 'gfx_background_nebula',
  GFX_MISSILE = 'gfx_missile',
  GFX_MISSILE_GUIDED = 'gfx_missile_guided'
}

export enum Names {
  PLAYER_SHIP = 'player_ship',
  ASTEROID = 'asteroid',
  ENEMY_UFO = 'enemy_ufo',
  ENEMY_INTERCEPTOR = 'enemy_interceptor',
  ENEMY_BOMBER = 'enemy_bomber',
  LASER_PROJECTILE = 'laser_projectile',
  MISSILE = 'missile',
  EXPLOSION = 'explosion',
  COLLECTIBLE_ITEM = 'collectible_item',
  LIVE_ICON = 'live',
  SCORE_DISPLAY = 'score_display',
  HIGHSCORE = 'highscore',
  BACKGROUND = 'background',
  PARTICLE_CONTAINER = 'particle_container',
  TEXT = 'text'
}

export enum zIndex {
  DEFAULT = 0,
  BACKGROUND = -10,
  ON_TOP = +10,
  PLAYER_SHIP = +1
}

export enum AppState {
  INITIALIZATION = 1,
  MENU = 2,
  PLAY_GAME = 3,
  SCORE_SCREEN = 4
}

export enum Spritesheets {
  EXPLOSION = 'explosion'
}

/**
 * Game constants and game objects data
 */

export const DEFAULT_GAME_SPEED = 1;
export const DEFAULT_PLAYER_SHIP_SPEED = 0.3;
export const STARTING_PLAYER_LIVES = 3;
export const MAX_PLAYER_LIVES = 5;
export const PLAYER_SHOT_COOLDOWN = 400;
export const SHIELD_DURATION = 5000;
export const ASTEROID_SPEED_MIN = 90;
export const ASTEROID_SPEED_MAX = 110;
export const ITEMS_SPEED = 100;
export const PLAYER_SHIP_HITPOINTS = 2;
export const SHIP_IMPACT_COLLISION_DMG = 4;
export const ENEMY_UFO_HITPOINTS = 4;
export const ENEMY_UFO_SCORE_REWARD = 5;
export const ENEMY_INTERCEPTOR_HITPOINTS = 2;
export const ENEMY_INTERCEPTOR_SCORE_REWARD = 1;
export const INTERCEPTOR_SPEED = 90;
export const ENEMY_BOMBER_HITPOINTS = 4;
export const ENEMY_BOMBER_SCORE_REWARD = 5;
export const BOMBER_SPEED = 85;
export const MISSILE_SPEED = 300;
export const MISSILE_GUIDED_MAX_SPEED = 450;
export const MISSILE_IMPACT_DMG = 4;
export const PLAYER_SHIP_RESPAWN_TIME = 1000;
export const GAME_OVER_DISPLAY_TIME = 2500;

export enum ExplosionType {
  LARGE = 'explosion_large',
  SMALL = 'explosion_small'
}

export enum AsteroidSize {
  LARGE = 'big',
  MEDIUM = 'med',
  SMALL = 'small'
}

export enum AsteroidType {
  BROWN = 'Brown',
  GREY = 'Grey'
}

export const AsteroidVariants = (size: AsteroidSize) => {
  switch (size) {
    case AsteroidSize.SMALL:
    case AsteroidSize.MEDIUM:
      return 2;
    case AsteroidSize.LARGE:
      return 4;
  }
}

export const AsteroidHitpoints = (size: AsteroidSize) => {
  switch (size) {
    case AsteroidSize.SMALL:
      return 1;
    case AsteroidSize.MEDIUM:
      return 2;
    case AsteroidSize.LARGE:
      return 4;
  }
}

export const AsteroidImpactDmg = (size: AsteroidSize) => {
  switch (size) {
    case AsteroidSize.SMALL:
      return 1;
    case AsteroidSize.MEDIUM:
      return 2;
    case AsteroidSize.LARGE:
      return 4;
  }
}

export enum MissileType {
  NORMAL = 1, 
  GUIDED = 2
}

export enum LaserColor {
  BLUE = 'Blue',
  RED = 'Red',
  GREEN = 'Green'
}

export enum LaserType {
  NORMAL = 1, 
  HEAVY = 2,
  SLOW = 3,
  SLOW_HEAVY = 4
}

export const LaserSpeed = (type: LaserType) => {
  switch (type) {
    case LaserType.NORMAL:
      return 750;
    case LaserType.HEAVY:
      return 700;
    case LaserType.SLOW:
      return 400;
    case LaserType.SLOW_HEAVY:
      return 350;
  }
}

export const LaserImpactDmg = (type: LaserType) => {
  switch (type) {
    case LaserType.NORMAL:
    case LaserType.SLOW:
      return 2;
    case LaserType.HEAVY:
    case LaserType.SLOW_HEAVY:
      return 4;
  }
}

export const LaserTexture = (color: LaserColor, type: LaserType) => {
  if (color === LaserColor.GREEN) { // textures for green lasers are in different order
    switch (type) {
      case LaserType.NORMAL:
        return 'laserGreen11.png';
      case LaserType.HEAVY:
        return 'laserGreen10.png';
      case LaserType.SLOW:
        return 'laserGreen13.png';
      case LaserType.SLOW_HEAVY:
        return 'laserGreen12.png';
    }
  } else { // blue, red
    switch (type) {
      case LaserType.NORMAL:
        return 'laser' + color + '01.png';
      case LaserType.HEAVY:
        return 'laser' + color + '16.png';
      case LaserType.SLOW:
        return 'laser' + color + '07.png';
      case LaserType.SLOW_HEAVY:
        return 'laser' + color + '06.png';
    }
  }
}

/**
 * UI Texts
 */
export enum Strings {
  GAME_NAME = 'Space shooter',
  PROMPT_START_GAME = 'Press space to start new game',
  LOADING = 'Loading, please wait...',
  GAME_OVER = 'Game over',
  USER_SCORE = 'Your score:',
  HIGH_SCORE = 'High score:',
  RECORD = 'New high score',
  PROMPT_CONTINUE = 'Press space to continue'
}
