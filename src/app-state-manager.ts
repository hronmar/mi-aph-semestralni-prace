import * as ECSA from '../libs/pixi-component';
import { Attributes, AppState } from './constants'
import { Factory } from './factory'
import { MenuComponent } from './global-components/menu-component'
import { GameComponent } from './global-components/game-component'
import { InputManager } from './global-components/input-manager';
import { GameObjectSpawner } from './global-components/game-object-spawner';
import { Random } from '../libs/pixi-math';
import { RNGSpinnerComponent } from './global-components/rng-spinner-component';
import { CollisionManager } from './global-components/collision-manager';
import { CollisionResolver } from './global-components/collision-resolver';
import { ObjectDestructionManager } from './global-components/object-destruction-manager';
import { OutsideSceneObjectsDisposer } from './global-components/outside-scene-objects-disposer';
import { LivesDisplayManager } from './global-components/lives-display-manager';
import { AudioComponent } from './global-components/audio-component';
import { ScoreDisplayManager } from './global-components/score-display-manager';
import { BackgroundRenderer } from './global-components/background-renderer';
import { PowerUpsManager } from './global-components/power-ups-manager';
import { ScoreScreenComponent } from './global-components/score-screen-component';

/**
 * Manages application state and allows switching between states
 */
export class AppStateManager {
  scene: ECSA.Scene;
  state: AppState;
  factory: Factory;

  constructor(scene: ECSA.Scene, factory: Factory) {
    this.scene = scene;
    this.state = AppState.INITIALIZATION;
    this.factory = factory;
  }

  /**
   * Switches to given state. If app is already in this state, 
   * restarts the state. 
   */
  switchToState(newState: AppState, data?: any) {
    // clear the scene and set global attributes
    this.scene.clearScene();
    this.scene.assignGlobalAttribute(Attributes.FACTORY, this.factory);
    this.scene.assignGlobalAttribute(Attributes.APP_STATE_MANAGER, this);
    let randomGenerator = new Random(Math.random() * Math.pow(2, 32));
    this.scene.assignGlobalAttribute(Attributes.RANDOM_GENERATOR, randomGenerator);

    // add global components
    let inputManager = new InputManager();
    this.scene.addGlobalComponent(inputManager);
    this.scene.assignGlobalAttribute(Attributes.INPUT_MANAGER, inputManager);

    // each application state has one global component that orchestrates its main logic (and performs initializations etc.)
    switch (newState) {
      case AppState.MENU:
        this.scene.addGlobalComponent(new MenuComponent());
        break;
      case AppState.PLAY_GAME:
        this.scene.addGlobalComponent(new GameComponent());

        // add all required global components
        this.scene.addGlobalComponent(new BackgroundRenderer());
        this.scene.addGlobalComponent(new GameObjectSpawner());
        this.scene.addGlobalComponent(new RNGSpinnerComponent());
        this.scene.addGlobalComponent(new CollisionManager());
        this.scene.addGlobalComponent(new CollisionResolver());
        this.scene.addGlobalComponent(new PowerUpsManager());
        this.scene.addGlobalComponent(new ObjectDestructionManager());
        this.scene.addGlobalComponent(new OutsideSceneObjectsDisposer());
        this.scene.addGlobalComponent(new LivesDisplayManager());
        this.scene.addGlobalComponent(new ScoreDisplayManager());
        this.scene.addGlobalComponent(new AudioComponent());
        break;
      case AppState.SCORE_SCREEN:
        this.scene.addGlobalComponent(new ScoreScreenComponent(data as number));
        break;
      default:
        // do nothing
        break;
    }

    this.state = newState;
  }
}
