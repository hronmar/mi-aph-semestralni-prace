import * as ECSA from '../libs/pixi-component';
import { Attributes, Strings, SCENE_WIDTH, SCENE_HEIGHT, Names, ExplosionType, Spritesheets, Flags, zIndex, AsteroidSize, AsteroidType, AsteroidVariants, Messages, LaserColor, Assets, SHIELD_DURATION, PLAYER_SHIP_HITPOINTS, SHIP_IMPACT_COLLISION_DMG, AsteroidImpactDmg, AsteroidHitpoints, ENEMY_UFO_HITPOINTS, ENEMY_UFO_SCORE_REWARD, ENEMY_INTERCEPTOR_HITPOINTS, LaserType, LaserTexture, LaserImpactDmg, ENEMY_INTERCEPTOR_SCORE_REWARD, ENEMY_BOMBER_HITPOINTS, ENEMY_BOMBER_SCORE_REWARD, MissileType, MISSILE_IMPACT_DMG, MISSILE_SPEED, MISSILE_GUIDED_MAX_SPEED } from './constants'
import { PlayerShipInputController } from './components/player-ship-controller';
import { TextureAtlas } from './texture-atlas';
import DynamicsComponent from './components/dynamics-component';
import Dynamics from './utils/dynamics';
import { Collidable } from './components/collidable';
import { Destructible } from './components/destructible';
import { PlayerDeathTrigger } from './components/player-death-trigger';
import { ExplosionTrigger } from './components/explosion-trigger';
import { ExplosionAnimator } from './components/explosion-animator';
import { AsteroidShatterTrigger } from './components/asteroid-shatter-trigger';
import { Random } from '../libs/pixi-math';
import { WanderSteeringComponent, CompoundSteeringComponent, SeekSteeringComponent, PursuitSteeringComponent, SteeringComponent } from './components/steering-components';
import { ForceBoundsComponent } from './components/force-bounds-component';
import { GiveScoreTrigger } from './components/give-score-trigger';
import { BlinkComponent, RotationComponent } from './components/misc-effects-components';
import { LaserFireComponent } from './components/laser-fire-component';
import { CollectibleItem, Collectible } from './components/collectible';
import { ShieldComponent } from './components/shield-component';
import { DropCollectibleItemTrigger } from './components/drop-collectible-item-trigger';
import { MissileFireComponent } from './components/missile-fire-component';
import { pointToVector } from './utils/functions';

/**
 * Factory class to simplify game objects construction
 */
export class Factory {
  private textureAtlas: TextureAtlas;
  private spritesheets: Map<string, PIXI.Spritesheet>;

  constructor(textureAtlas: TextureAtlas, spritesheets: Map<string, PIXI.Spritesheet>) {
    this.textureAtlas = textureAtlas;
    this.spritesheets = spritesheets;
  }

  createMainMenu(scene: ECSA.Scene) {
    this.createBackground(scene);

    let builder = new ECSA.Builder(scene);

    // caption with game name
    builder
      .localPos(SCENE_WIDTH / 2, SCENE_HEIGHT / 3)
      .anchor(0.5)
      .withParent(scene.stage)
      .asText(Names.TEXT, Strings.GAME_NAME,
        new PIXI.TextStyle({ fill: '#DD0000', fontSize: 70, fontFamily: 'Courier New' }))
      .build();

    // press key prompt
    builder
      .localPos(SCENE_WIDTH / 2, 2 * SCENE_HEIGHT / 3)
      .anchor(0.5)
      .withParent(scene.stage)
      .withComponent(new BlinkComponent(1000, 750))
      .asText(Names.TEXT, Strings.PROMPT_START_GAME,
        new PIXI.TextStyle({ fill: '#FF0000', fontSize: 32, fontFamily: 'Courier New' }))
      .build();
  }

  createPlayerShip(scene: ECSA.Scene, giveProtection: boolean) {
    let bld = new ECSA.Builder(scene);
    bld
      .localPos(SCENE_WIDTH / 2, 3 * SCENE_HEIGHT / 4)
      .anchor(0.5)
      .withParent(scene.stage)
      .withComponent(new Collidable(SHIP_IMPACT_COLLISION_DMG))
      .withComponent(new Destructible(PLAYER_SHIP_HITPOINTS))
      .withComponent(new PlayerShipInputController())
      .withComponent(new ForceBoundsComponent())
      .withComponent(new PlayerDeathTrigger());
    if (giveProtection)
      bld.withComponent(new ShieldComponent(SHIELD_DURATION));
    bld
      .withComponent(new ExplosionTrigger())
      .asSprite(this.textureAtlas.getTexture('playerShip1_blue.png'), Names.PLAYER_SHIP)
      .build().zIndex = zIndex.PLAYER_SHIP;
  }

  createAsteroid(scene: ECSA.Scene, position: ECSA.Vector, dynamics: Dynamics,
    rotation: number, size: AsteroidSize, type: AsteroidType) {
    let variant = this.getRNG(scene).uniformInt(1, AsteroidVariants(size));
    let texName = 'meteor' + type + '_' + size + '' + variant + '.png';
    let obj = new ECSA.Builder(scene)
      .localPos(position.x, position.y)
      .anchor(0.5)
      .withParent(scene.stage)
      .withComponent(new Collidable(AsteroidImpactDmg(size)))
      .withComponent(new Destructible(AsteroidHitpoints(size)))
      .withAttribute(Attributes.DYNAMICS, dynamics)
      .withComponent(new DynamicsComponent(Attributes.DYNAMICS))
      .withComponent(new AsteroidShatterTrigger(size, type))
      .withFlag(Flags.REMOVE_OUTSIDE_SCENE)
      .asSprite(this.textureAtlas.getTexture(texName), Names.ASTEROID)
      .build();
    obj.rotation = rotation;
  }

  createEnemyUFO(scene: ECSA.Scene, position: ECSA.Vector, target: ECSA.Vector,
    drop?: CollectibleItem) {
    let maxAcc = 30;
    let maxSpeed = 100;
    let texture = (this.getRNG(scene).uniform(0, 1) <= 0.5 ? 'ufoRed.png' : 'ufoYellow.png');
    let steerWander = new WanderSteeringComponent(maxAcc, maxSpeed, false, 20, 65, 0.8);
    let steerSeek = new SeekSteeringComponent(maxAcc, maxSpeed, false, target, true);
    let steerCompound = new CompoundSteeringComponent(maxAcc, maxSpeed, false);
    steerCompound.addSteeringComponent(steerWander, 1.);
    steerCompound.addSteeringComponent(steerSeek, 0.5);
    let bld = new ECSA.Builder(scene);
    bld
      .localPos(position.x, position.y)
      .anchor(0.5)
      .withParent(scene.stage)
      .withComponent(new Collidable(SHIP_IMPACT_COLLISION_DMG))
      .withComponent(new Destructible(ENEMY_UFO_HITPOINTS))
      .withComponent(new LaserFireComponent(0.4, LaserType.HEAVY, LaserColor.RED))
      .withComponent(new DynamicsComponent(Attributes.DYNAMICS))
      .withComponent(steerWander)
      .withComponent(steerSeek)
      .withComponent(steerCompound)
      .withComponent(new ExplosionTrigger())
      .withComponent(new GiveScoreTrigger(ENEMY_UFO_SCORE_REWARD))
      .withComponent(new RotationComponent(0.1))
      .withFlag(Flags.REMOVE_OUTSIDE_SCENE);
    if (drop != null) {
      bld.withComponent(new DropCollectibleItemTrigger(drop));
    }
    bld
      .asSprite(this.textureAtlas.getTexture(texture), Names.ENEMY_UFO)
      .build();
  }

  createEnemyInterceptor(scene: ECSA.Scene, position: ECSA.Vector, dynamics: Dynamics,
    rotation: number, variant: number, drop?: CollectibleItem) {
    let texName = 'enemyBlack' + variant + '.png';
    let bld = new ECSA.Builder(scene);
    bld
      .localPos(position.x, position.y)
      .anchor(0.5)
      .withParent(scene.stage)
      .withComponent(new Collidable(SHIP_IMPACT_COLLISION_DMG))
      .withComponent(new Destructible(ENEMY_INTERCEPTOR_HITPOINTS))
      .withComponent(new LaserFireComponent(0.4, LaserType.SLOW, LaserColor.GREEN, Math.PI / 2))
      .withAttribute(Attributes.DYNAMICS, dynamics)
      .withComponent(new DynamicsComponent(Attributes.DYNAMICS))
      .withComponent(new ExplosionTrigger())
      .withComponent(new GiveScoreTrigger(ENEMY_INTERCEPTOR_SCORE_REWARD))
      .withFlag(Flags.REMOVE_OUTSIDE_SCENE);
    if (drop != null) {
      bld.withComponent(new DropCollectibleItemTrigger(drop));
    }
    bld
      .asSprite(this.textureAtlas.getTexture(texName), Names.ENEMY_INTERCEPTOR)
      .build()
      .rotation = rotation;
  }

  createEnemyBomber(scene: ECSA.Scene, position: ECSA.Vector, dynamics: Dynamics,
    rotation: number, variant: number, drop?: CollectibleItem,
    laserAttack: boolean = true, missileAttack: MissileType = null) {
    let texName = (missileAttack != null ? 'enemyRed' : 'enemyBlack') + (variant + 3) + '.png';
    let bld = new ECSA.Builder(scene);
    bld
      .localPos(position.x, position.y)
      .anchor(0.5)
      .withParent(scene.stage)
      .withComponent(new Collidable(SHIP_IMPACT_COLLISION_DMG))
      .withComponent(new Destructible(ENEMY_BOMBER_HITPOINTS));
    if (laserAttack) {
      bld.withComponent(new LaserFireComponent(0.7, LaserType.SLOW_HEAVY, LaserColor.RED, Math.PI / 2));
    }
    if (missileAttack != null) {
      bld.withComponent(new MissileFireComponent(
        missileAttack === MissileType.NORMAL ? 0.4 : 0.3, missileAttack));
    }
    bld.withAttribute(Attributes.DYNAMICS, dynamics)
      .withComponent(new DynamicsComponent(Attributes.DYNAMICS))
      .withComponent(new ExplosionTrigger())
      .withComponent(new GiveScoreTrigger(ENEMY_BOMBER_SCORE_REWARD))
      .withFlag(Flags.REMOVE_OUTSIDE_SCENE);
    if (drop != null) {
      bld.withComponent(new DropCollectibleItemTrigger(drop));
    }
    bld
      .asSprite(this.textureAtlas.getTexture(texName), Names.ENEMY_BOMBER)
      .build()
      .rotation = rotation;
  }

  createExplosion(scene: ECSA.Scene, position: ECSA.Vector, explosionType: ExplosionType) {
    let sheet = this.spritesheets.get(Spritesheets.EXPLOSION);
    let anim = sheet.animations[explosionType];
    let obj = new ECSA.Builder(scene)
      .localPos(position.x, position.y)
      .anchor(0.5)
      .withParent(scene.stage)
      .withComponent(new ExplosionAnimator(anim))
      .asSprite(anim[0], Names.EXPLOSION)
      .build();
    obj.zIndex = zIndex.ON_TOP;
  }

  createLaserProjectile(scene: ECSA.Scene, position: ECSA.Vector, dynamics: Dynamics, source: ECSA.Container,
    rotation: number = 0, laserColor: LaserColor = LaserColor.BLUE, laserType: LaserType = LaserType.NORMAL) {
    let texName = LaserTexture(laserColor, laserType);
    let obj = new ECSA.Builder(scene)
      .localPos(position.x, position.y)
      .anchor(0.5)
      .withParent(scene.stage)
      .withComponent(new Collidable(LaserImpactDmg(laserType)))
      .withComponent(new Destructible(1))
      .withAttribute(Attributes.DYNAMICS, dynamics)
      .withComponent(new DynamicsComponent(Attributes.DYNAMICS))
      .withFlag(Flags.REMOVE_OUTSIDE_SCENE)
      .asSprite(this.textureAtlas.getTexture(texName), Names.LASER_PROJECTILE)
      .build();
    obj.rotation = rotation;
    obj.zIndex = zIndex.ON_TOP;
    scene.sendMessage(new ECSA.Message(Messages.LASER_PROJECTILE_FIRED, null, null, { source: source, type: laserType }));
  }

  createMissile(scene: ECSA.Scene, position: ECSA.Vector, dynamics: Dynamics,
    target: ECSA.Container, missileType: MissileType = MissileType.NORMAL) {
    let steerComp: SteeringComponent;
    let texture: PIXI.Texture;
    if (missileType === MissileType.NORMAL) {
      steerComp = new SeekSteeringComponent(40, MISSILE_SPEED, true, pointToVector(target.position), true);
      texture = PIXI.Texture.from(Assets.GFX_MISSILE);
    } else { // guided missile
      steerComp = new PursuitSteeringComponent(60, MISSILE_GUIDED_MAX_SPEED, true, target);
      texture = PIXI.Texture.from(Assets.GFX_MISSILE_GUIDED);
    }
    let bld = new ECSA.Builder(scene)
      .localPos(position.x, position.y)
      .anchor(0.5)
      .withParent(scene.stage)
      .withComponent(new Collidable(MISSILE_IMPACT_DMG))
      .withComponent(new Destructible(1))
      .withAttribute(Attributes.DYNAMICS, dynamics)
      .withComponent(new DynamicsComponent(Attributes.DYNAMICS))
      .withComponent(steerComp)
      .withComponent(new ExplosionTrigger(ExplosionType.SMALL));
    if (missileType === MissileType.GUIDED) {
      bld.withComponent(new ECSA.GenericComponent('playerDeathWatcher')
        .doOnMessage(Messages.PLAYER_KILLED, (cmp: ECSA.Component, msg: ECSA.Message) => {
          // when player is killed -> explode
          cmp.owner.findComponentByName<Destructible>(Destructible.name).impactBy(cmp.owner);
        }));
    }
    bld.withFlag(Flags.REMOVE_OUTSIDE_SCENE)
      .asSprite(texture, Names.MISSILE)
      .build().zIndex = zIndex.ON_TOP;
    scene.sendMessage(new ECSA.Message(Messages.MISSILE_LAUNCHED, null, null, missileType));
  }

  createCollectibleItem(scene: ECSA.Scene, position: ECSA.Vector, dynamics: Dynamics,
    item: CollectibleItem) {
    let texName: string;
    switch (item) {
      case CollectibleItem.SHIELD_POWER_UP:
        texName = 'powerupBlue_shield.png';
        break;
      case CollectibleItem.BONUS_LIVE:
        texName = 'powerupGreen_bolt.png';
        break;
      case CollectibleItem.BONUS_SCORE:
        texName = 'powerupYellow_star.png';
        break;
    }
    new ECSA.Builder(scene)
      .localPos(position.x, position.y)
      .anchor(0.5)
      .withParent(scene.stage)
      .withComponent(new Collidable())
      .withComponent(new Collectible(item))
      .withAttribute(Attributes.DYNAMICS, dynamics)
      .withComponent(new DynamicsComponent(Attributes.DYNAMICS))
      .withFlag(Flags.REMOVE_OUTSIDE_SCENE)
      .asSprite(this.textureAtlas.getTexture(texName), Names.COLLECTIBLE_ITEM)
      .build();
  }

  createLiveDisplayIcon(scene: ECSA.Scene, position: ECSA.Vector, name: string) {
    let obj = new ECSA.Builder(scene)
      .localPos(position.x, position.y)
      .anchor(0.5)
      .withParent(scene.stage)
      .asSprite(this.textureAtlas.getTexture('playerLife1_blue.png'), name)
      .build();
    obj.zIndex = zIndex.ON_TOP;
  }

  createScoreDisplay(scene: ECSA.Scene, position: ECSA.Vector) {
    let text = new ECSA.Builder(scene)
      .localPos(position.x, position.y)
      .anchor(0.5)
      .withParent(scene.stage)
      .asText(Names.SCORE_DISPLAY, '',
        new PIXI.TextStyle({ fill: '#FF0000', fontSize: 30, fontFamily: 'Courier New' }))
      .build();
    text.zIndex = zIndex.ON_TOP;
  }

  createGameOverAnnouncement(scene: ECSA.Scene) {
    let text = new ECSA.Builder(scene)
      .localPos(SCENE_WIDTH / 2, SCENE_HEIGHT / 3)
      .anchor(0.5)
      .withParent(scene.stage)
      .asText(Names.TEXT, Strings.GAME_OVER,
        new PIXI.TextStyle({ fill: '#FF0000', fontSize: 70, fontFamily: 'Courier New' }))
      .build();
    text.zIndex = zIndex.ON_TOP;
  }

  createBackground(scene: ECSA.Scene) {
    let builder = new ECSA.Builder(scene);

    let starfield = builder
      .localPos(0, 0)
      .withParent(scene.stage)
      .asSprite(PIXI.Texture.from(Assets.GFX_BACKGROUND_STARS), Names.BACKGROUND)
      .build();
    starfield.width = SCENE_WIDTH;
    starfield.height = SCENE_HEIGHT;
    starfield.zIndex = zIndex.BACKGROUND;

    let nebula = builder
      .localPos(0, 0)
      .withParent(scene.stage)
      .asSprite(PIXI.Texture.from(Assets.GFX_BACKGROUND_NEBULA), Names.BACKGROUND)
      .build();
    nebula.width = SCENE_WIDTH;
    nebula.height = SCENE_HEIGHT;
    nebula.zIndex = zIndex.BACKGROUND;
  }

  createDustParticleContainer(scene: ECSA.Scene) {
    let particleContainer = new ECSA.Builder(scene)
      .localPos(0, 0)
      .withParent(scene.stage)
      .asContainer(Names.PARTICLE_CONTAINER)
      .build();
    particleContainer.zIndex = zIndex.BACKGROUND;
    particleContainer.sortableChildren = false;
  }

  createDustParticle(scene: ECSA.Scene, position: ECSA.Vector, dynamics: Dynamics) {
    let particleContainer = scene.findObjectByName(Names.PARTICLE_CONTAINER);
    let particle = new ECSA.Graphics();

    particle.lineStyle(0);
    particle.beginFill(0xA1A1A1, this.getRNG(scene).normal(0.75, 1));
    particle.drawCircle(0, 0, this.getRNG(scene).normal(0.25, 1.25));
    particle.endFill();

    new ECSA.Builder(scene)
      .localPos(position.x, position.y)
      .withAttribute(Attributes.DYNAMICS, dynamics)
      .withComponent(new DynamicsComponent(Attributes.DYNAMICS))
      .withParent(particleContainer)
      .buildInto(particle);
  }

  createScoreScreenDisplay(scene: ECSA.Scene, score: number, highscore: number, record: boolean) {
    this.createBackground(scene);
    let builder = new ECSA.Builder(scene);
    builder
      .localPos(SCENE_WIDTH / 2, SCENE_HEIGHT / 4)
      .anchor(0.5)
      .withParent(scene.stage)
      .asText(Names.TEXT, Strings.GAME_OVER,
        new PIXI.TextStyle({ fill: '#DD0000', fontSize: 70, fontFamily: 'Courier New' }))
      .build();
    builder
      .localPos(SCENE_WIDTH / 2, SCENE_HEIGHT / 2)
      .anchor(0.5)
      .withParent(scene.stage)
      .asText(Names.TEXT, Strings.USER_SCORE + ' ' + score,
        new PIXI.TextStyle({ fill: '#DD0000', fontSize: 30, fontFamily: 'Courier New' }))
      .build();
    builder
      .localPos(SCENE_WIDTH / 2, SCENE_HEIGHT / 2 + 50)
      .anchor(0.5)
      .withParent(scene.stage)
      .asText(Names.TEXT, Strings.HIGH_SCORE + ' ' + highscore,
        new PIXI.TextStyle({ fill: '#DD0000', fontSize: 30, fontFamily: 'Courier New' }))
      .build();
    if (record) {
      builder
        .localPos(SCENE_WIDTH / 2, SCENE_HEIGHT / 2 - 50)
        .anchor(0.5)
        .withParent(scene.stage)
        .withComponent(new BlinkComponent(750, 500))
        .asText(Names.TEXT, Strings.RECORD,
          new PIXI.TextStyle({ fill: '#DD0000', fontSize: 30, fontFamily: 'Courier New' }))
        .build();
    }
    builder
      .localPos(SCENE_WIDTH / 2, 3 * SCENE_HEIGHT / 4)
      .anchor(0.5)
      .withParent(scene.stage)
      .asText(Names.TEXT, Strings.PROMPT_CONTINUE,
        new PIXI.TextStyle({ fill: '#FF0000', fontSize: 32, fontFamily: 'Courier New' }))
      .build();
  }

  private getRNG(scene: ECSA.Scene): Random {
    return scene.getGlobalAttribute(Attributes.RANDOM_GENERATOR);
  }
}
